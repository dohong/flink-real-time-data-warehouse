package com.jolohong.Ware.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jolohong.Ware.app.function.DimAsyncFunction;
import com.jolohong.Ware.been.OrderDetail;
import com.jolohong.Ware.been.OrderInfo;
import com.jolohong.Ware.been.OrderWide;
import com.jolohong.Ware.utils.DateTimeUtil;
import com.jolohong.Ware.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class OrderWideApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setParallelism(1);
        String orderInfoSourceTopic = "dwd_order_info";
        String orderDetailSourceTopic = "dwd_order_detail";
        String orderWideSinkTopic = "dwm_order_wide";
        String groupId = "order_wide_group";
        SingleOutputStreamOperator<OrderInfo> orderInfoDS = executionEnvironment.addSource(KafkaUtil.getKafkaConsumer(orderInfoSourceTopic, groupId)).map(line -> {
            OrderInfo orderInfo = JSON.parseObject(line, OrderInfo.class);
            String create_time = DateTimeUtil.toYMDhms(orderInfo.getCreate_time());
            String[] dateTimeArr = create_time.split(" ");
            orderInfo.setCreate_date(dateTimeArr[0]);
            orderInfo.setCreate_hour(dateTimeArr[1].split(":")[0]);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            orderInfo.setCreate_ts(simpleDateFormat.parse(create_time).getTime());
            return orderInfo;
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<OrderInfo>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<OrderInfo>() {
                    @Override
                    public long extractTimestamp(OrderInfo element, long recordTimestamp) {
                        return element.getCreate_ts();
                    }
                })
        );
        SingleOutputStreamOperator<OrderDetail> orderDetailDS = executionEnvironment.addSource(KafkaUtil.getKafkaConsumer(orderDetailSourceTopic, groupId)).map(
                line -> {
                    OrderDetail orderDetail = JSON.parseObject(line, OrderDetail.class);
                    String create_time = DateTimeUtil.toYMDhms(orderDetail.getCreate_time());

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    orderDetail.setCreate_ts(simpleDateFormat.parse(create_time).getTime());
//                    orderDetail.setCreate_ts(DateTimeUtil.checkTs(create_time));
                    return orderDetail;
                }
        ).assignTimestampsAndWatermarks(WatermarkStrategy.<OrderDetail>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<OrderDetail>() {
                    @Override
                    public long extractTimestamp(OrderDetail orderDetail, long l) {
                        return orderDetail.getCreate_ts();
                    }
                })
        );
        //TODO 3.双流JOIN
        SingleOutputStreamOperator<OrderWide> orderWideWithNoDimDS = orderInfoDS.keyBy(OrderInfo::getId).intervalJoin(orderDetailDS.keyBy(OrderDetail::getOrder_id))
                .between(Time.seconds(-59), Time.seconds(59))
                .process(new ProcessJoinFunction<OrderInfo, OrderDetail, OrderWide>() {

                    @Override
                    public void processElement(OrderInfo orderInfo, OrderDetail orderDetail, ProcessJoinFunction<OrderInfo, OrderDetail, OrderWide>.Context context, Collector<OrderWide> collector) throws Exception {
                        collector.collect(new OrderWide(orderInfo, orderDetail));
                    }
                });
        orderWideWithNoDimDS.print("<------NODIMS-------->");
        //TODO 4.关联维度信息  HBase Phoenix
//        orderWideWithNoDimDS.map(orderWide -> {
//            //关联用户维度
//            Long user_id = orderWide.getUser_id();
//            //根据user_id查询Phoenix用户信息
//            //将用户信息补充至orderWide
//            //地区
//            //SKU
//            //SPU
//            //。。。
//            //返回结果
//            return orderWide;
//        });
        //4.1 关联用户维度
        SingleOutputStreamOperator<OrderWide> orderWidWithUserDS = AsyncDataStream.unorderedWait(
                orderWideWithNoDimDS, new DimAsyncFunction<OrderWide>("DIM_USER_INFO") {
                    @Override
                    public String getKey(OrderWide input) {
                        return input.getUser_id().toString();
                    }

                    @Override
                    public void join(OrderWide input, JSONObject dimInfo) throws ParseException {
                        input.setUser_gender(dimInfo.getString("GENDER"));

                        String birthday = dimInfo.getString("BIRTHDAY");
//                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        long currentTs = System.currentTimeMillis();
//                        long ts = simpleDateFormat.parse(birthday).getTime();
                        long ts = Long.parseLong(birthday);
                        long age = (currentTs - ts) / (1000 * 60 * 60 * 24 * 365L);
                        input.setUser_age((int) age);

                    }
                }, 60,
                TimeUnit.SECONDS
        );
        //4.2 关联地区维度
        SingleOutputStreamOperator<OrderWide> orderWideWithProvinceDS = AsyncDataStream.unorderedWait(
                orderWidWithUserDS,
                new DimAsyncFunction<OrderWide>("DIM_BASE_PROVINCE") {
                    @Override
                    public String getKey(OrderWide input) {
                        return input.getProvince_id().toString();
                    }

                    @Override
                    public void join(OrderWide input, JSONObject dimInfo) throws ParseException {
                        input.setProvince_name(dimInfo.getString("NAME"));
                        input.setProvince_area_code(dimInfo.getString("AREA_CODE"));
                        input.setProvince_iso_code(dimInfo.getString("ISO_CODE"));
                        input.setProvince_3166_2_code(dimInfo.getString("ISO_3166_2"));
                    }

                    @Override
                    public void timeout(OrderWide input, ResultFuture<OrderWide> resultFuture) throws Exception {
//                        super.timeout(input, resultFuture);
                        System.out.println("timeout DIm_BASE_PROVICE "+input);
                    }
                },
                60, TimeUnit.SECONDS
        );
        //4.3 关联SKU维度
        SingleOutputStreamOperator<OrderWide> orderWideWithSkuDS = AsyncDataStream.unorderedWait(
                orderWideWithProvinceDS,
                new DimAsyncFunction<OrderWide>("DIM_SKU_INFO") {
                    @Override
                    public String getKey(OrderWide input) {
                        return input.getSku_id().toString();
                    }

                    @Override
                    public void join(OrderWide input, JSONObject dimInfo) throws ParseException {
                        System.out.println("SKU dim :" + dimInfo);
                        input.setSku_name(dimInfo.getString("SKU_NAME"));
                        input.setCategory3_id(dimInfo.getLong("CATEGORY3_ID"));
                        input.setSpu_id(dimInfo.getLong("SPU_ID"));
                        input.setTm_id(dimInfo.getLong("TM_ID"));
                    }
                },
                60, TimeUnit.SECONDS);

        //4.4 关联SPU维度
        SingleOutputStreamOperator<OrderWide> orderWideWithSpuDS = AsyncDataStream.unorderedWait(
                orderWideWithSkuDS,
                new DimAsyncFunction<OrderWide>("DIM_SPU_INFO") {
                    @Override
                    public String getKey(OrderWide input) {
                        return String.valueOf(input.getSpu_id());
                    }

                    @Override
                    public void join(OrderWide input, JSONObject dimInfo) throws ParseException {
                        input.setSpu_name(dimInfo.getString("SPU_NAME"));
                    }

                    @Override
                    public void timeout(OrderWide input, ResultFuture<OrderWide> resultFuture) throws Exception {
                        System.out.println("timeout DIM_SPU_INFO" + input +"   "+getKey(input));
                    }
                },
                60, TimeUnit.SECONDS);
        orderWideWithSpuDS.print("orderWideWithSpuDS.print");
        //4.5 关联TM维度
        SingleOutputStreamOperator<OrderWide> orderWideWithCategory3DS = AsyncDataStream.unorderedWait(orderWideWithSpuDS, new DimAsyncFunction<OrderWide>("DIM_BASE_TRADEMARK") {
                    @Override
                    public String getKey(OrderWide input) {
                        return String.valueOf(input.getCategory3_id());
                    }

                    @Override
                    public void join(OrderWide input, JSONObject dimInfo) throws ParseException {
                        input.setCategory3_name(dimInfo.getString("TM_NAME"));
                    }

                    @Override
                    public void timeout(OrderWide input, ResultFuture<OrderWide> resultFuture) throws Exception {
                        System.out.println("timeout DIM_BASE_TRADEMARK " + input +" id key:  "+input.getCategory3_id());
                    }
                },
                60, TimeUnit.SECONDS);
        orderWideWithCategory3DS.print("orderWideWithCategory3DS");
        orderWideWithCategory3DS.map(JSONObject::toJSONString).addSink(
                KafkaUtil.getKafkaProducer(orderWideSinkTopic)
        );
        executionEnvironment.execute("OrderWideApp");

    }
}
