package com.jolohong.Ware.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jolohong.Ware.been.OrderWide;
import com.jolohong.Ware.been.PaymentInfo;
import com.jolohong.Ware.been.PaymentWide;
import com.jolohong.Ware.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.text.SimpleDateFormat;

public class PaymentWideApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setParallelism(1);
        String groupId = "payment_wide_group";
        String paymentInfoSourceTopic = "dwd_payment_info";
        String orderWideSourceTopic = "dwm_order_wide";
        String paymentWideSinkTopic = "dwm_payment_wide";
        SingleOutputStreamOperator<OrderWide> orderWideDS = executionEnvironment.addSource(KafkaUtil.getKafkaConsumer(orderWideSourceTopic, groupId))
                .map(line -> JSON.parseObject(line, OrderWide.class))
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy.<OrderWide>forMonotonousTimestamps()
                                .withTimestampAssigner(new SerializableTimestampAssigner<OrderWide>() {
                                       @Override
                                       public long extractTimestamp(OrderWide orderWide, long l) {
                                           SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                           System.out.println("orderWide :" + orderWide);
                                           try {
                                               return Long.parseLong(orderWide.getCreate_time());
//                                               return simpleDateFormat.parse(orderWide.getCreate_time()).getTime();

                                           } catch (Exception e) {
                                               e.printStackTrace();
                                               return l;
                                           }
                                       }
                                   }
                                ));

        SingleOutputStreamOperator<PaymentInfo> paymentInfoDS = executionEnvironment.addSource(KafkaUtil.getKafkaConsumer(paymentInfoSourceTopic,groupId))
                        .map(line->JSON.parseObject(line,PaymentInfo.class))
                                .assignTimestampsAndWatermarks(WatermarkStrategy.<PaymentInfo>forMonotonousTimestamps()
                                        .withTimestampAssigner(new SerializableTimestampAssigner<PaymentInfo>() {
                                            @Override
                                            public long extractTimestamp(PaymentInfo paymentInfo, long l) {
                                                try {
                                                    System.out.println("paymentinfo :" + paymentInfo);
                                                    return Long.parseLong(paymentInfo.getCreate_time());
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    return l;
                                                }
                                            }
                                        }));
        SingleOutputStreamOperator<PaymentWide> paymentWideDS = paymentInfoDS.keyBy(PaymentInfo::getOrder_id).intervalJoin(
                orderWideDS.keyBy(OrderWide::getOrder_id)
        ).between(Time.seconds(-60*10), Time.seconds(600)
        ).process(
                new ProcessJoinFunction<PaymentInfo, OrderWide, PaymentWide>() {

                    @Override
                    public void processElement(PaymentInfo paymentInfo, OrderWide orderWide, ProcessJoinFunction<PaymentInfo, OrderWide, PaymentWide>.Context context, Collector<PaymentWide> collector) throws Exception {
                        System.out.println("-------------join>> orderWide: "+orderWide.getOrder_id()+"  payment: "+paymentInfo.getOrder_id());
                        collector.collect(new PaymentWide(paymentInfo, orderWide));
                    }
                }
        );
        paymentWideDS.print("paymentWideDS>>>>>>>>>");
        paymentWideDS.map(JSONObject::toJSONString).addSink(KafkaUtil.getKafkaProducer(paymentWideSinkTopic));
        executionEnvironment.execute("PaymentWideApp");

    }
}
