package com.jolohong.Ware.app.dws;

import com.jolohong.Ware.been.ProvinceStats;
import com.jolohong.Ware.utils.ClickHouseUtil;
import com.jolohong.Ware.utils.KafkaUtil;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableConfig;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

public class ProvinceStatsSqlApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setParallelism(1);
        executionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        executionEnvironment.setStateBackend(new FsStateBackend("file:///flink/checkpoints/ck"));
        executionEnvironment.enableCheckpointing(5000L);
        executionEnvironment.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        executionEnvironment.getCheckpointConfig().setCheckpointTimeout(10000L);
        executionEnvironment.getCheckpointConfig().setMaxConcurrentCheckpoints(2);
        executionEnvironment.getCheckpointConfig().setMinPauseBetweenCheckpoints(3000);

        executionEnvironment.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
        TableConfig tableConfig = new TableConfig();
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment streamTableEnvironment = StreamTableEnvironment.create(executionEnvironment, settings);

        String groupId = "province_stats";
        String orderWideTopic = "dwm_order_wide";
        streamTableEnvironment.executeSql("CREATE TABLE `order_wide` ( " +
                "  `province_id` BIGINT, " +
                "  `province_name` STRING, " +
                "  `province_area_code` STRING, " +
                "  `province_iso_code` STRING, " +
                "  `province_3166_2_code` STRING, " +
                "  `order_id` BIGINT, " +
                "  `split_total_amount` DECIMAL(16,2), " +
                "  `create_time` BIGINT, " +
                "   `create_ts` BIGINT," +
                "    proc_time as PROCTIME(), " +
                "  `rt` as TO_TIMESTAMP(FROM_UNIXTIME(create_time/1000, 'yyyy-MM-dd HH:mm:ss')), " +
//                "    rt as LOCALTIMESTAMP, " +
                "  WATERMARK FOR rt AS rt - INTERVAL '5' SECOND ) with(" +
                KafkaUtil.getKafkaDDL(orderWideTopic, groupId) + ")").print();

        Table table = streamTableEnvironment.sqlQuery("select " +
                "    DATE_FORMAT(TUMBLE_START(proc_time, INTERVAL '10' SECOND), 'yyyy-MM-dd HH:mm:ss') stt, " +
                "    DATE_FORMAT(TUMBLE_END(proc_time, INTERVAL '10' SECOND), 'yyyy-MM-dd HH:mm:ss') edt, " +
                "    province_id, " +
                "    province_name, " +
                "    province_area_code, " +
                "    province_iso_code, " +
                "    province_3166_2_code, " +
                "    count(distinct order_id) order_count, " +
                "    sum(split_total_amount) order_amount, " +
                "    UNIX_TIMESTAMP()*1000 ts " +
                "from " +
                "    order_wide " +
                "group by " +
                "    province_id, " +
                "    province_name, " +
                "    province_area_code, " +
                "    province_iso_code, " +
                "    province_3166_2_code, " +
                "    TUMBLE(proc_time, INTERVAL '10' SECOND)");
//        table.execute().print();

        DataStream<ProvinceStats> provinceStatsDataStream = streamTableEnvironment.toAppendStream(table, ProvinceStats.class);
        provinceStatsDataStream.print();
        provinceStatsDataStream.addSink(ClickHouseUtil.getSink("insert into gmalldb.province_stats_2022 values(?,?,?,?,?,?,?,?,?,?)"));


        executionEnvironment.execute("ProvinceStatsSqlApp");
    }
}
