package com.jolohong.Ware.app.function;

import com.alibaba.fastjson.JSONObject;
import com.jolohong.Ware.config.Config;
import com.jolohong.Ware.utils.DimUtil;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.concurrent.ThreadPoolExecutor;

import com.jolohong.Ware.utils.ThreadPoolUtil;

public class DimAsyncFunction<T> extends RichAsyncFunction<T, T> implements DimAsyncJoinFunction<T> {
    private String tableName;
    private Connection connection;
    private ThreadPoolExecutor threadPoolExecutor;

    public DimAsyncFunction(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName(Config.PHOENIX_DRIVER);
        connection= DriverManager.getConnection(Config.PHOENIX_SERVER);
        this.threadPoolExecutor=ThreadPoolUtil.getThreadPool();

    }

    @Override
    public void asyncInvoke(T t, ResultFuture<T> resultFuture) throws Exception {
        threadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //获取查询的主键
                    String key = getKey(t);
                    //查询维度信息
                    JSONObject dimInfo = DimUtil.getDimInfo(connection, tableName, key);
                    if(dimInfo!=null){
                        join(t,dimInfo);
                    }

                    resultFuture.complete(Collections.singletonList(t));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) throws Exception {
        System.out.println("TimeOut: "+input);
    }

    @Override
    public String getKey(T input) {
        return null;
    }

    @Override
    public void join(T input, JSONObject dimInfo) throws ParseException {

    }

}
