package com.jolohong.Ware.been;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class KeywordLog {
    JSONObject common;
    JSONObject page;
    JSONObject displays;

    String ts;
}
