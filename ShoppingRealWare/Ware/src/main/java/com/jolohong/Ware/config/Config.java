package com.jolohong.Ware.config;

import org.apache.calcite.util.Static;

public class Config {
    public static final String BOOSTARTSERVERS="ufbvunjvxypwdpuh:9092,ufbvunjvxypwdpuh-0002:9092,ufbvunjvxypwdpuh-0001:9092";
//    public static final String BOOSTARTSERVERS="master:9092,slave001:9092,slave002:9092";
    //Phoenix库名
    public static final String HBASE_SCHEMA = "GMALL_REALTIME";

    //Phoenix驱动
    public static final String PHOENIX_DRIVER = "org.apache.phoenix.jdbc.PhoenixDriver";

    //Phoenix连接参数
    public static final String PHOENIX_SERVER = "jdbc:phoenix:ufbvunjvxypwdpuh,ufbvunjvxypwdpuh-0002,ufbvunjvxypwdpuh-0001:2181";
//    public static final String PHOENIX_SERVER ="jdbc:phoenix:master:2181";
    //ClickHouse_Driver
    public static final String CLICKHOUSE_DRIVER = "ru.yandex.clickhouse.ClickHouseDriver";

    //ClickHouse_Url
    public static final String CLICKHOUSE_URL = "jdbc:clickhouse://ufbvunjvxypwdpuh-0002:8123/default";

    //redis
    public static final String REDISHOST="ufbvunjvxypwdpuh";
//    public static final String REDISHOST="slave001";
    public static final Integer REDISPOST=6379;
    public static final String REDISAUTH="jolohong";

    // CONFIG TAG MYSQL
    public static final String CONFUSERNAME="root";
    public static final String CONFPASSWD="root";
    public static final String CONFHOST="ufbvunjvxypwdpuh-0002";
    public static final String CONFDATABAS="statemanager";
    public static final String CONFTABLELISTS="statemanager.table_process";

    // ods mysqlDB
    public static final String ODSUSERNAME="root";
    public static final String ODSPASSWD="root";
    public static final String ODSHOST="ufbvunjvxypwdpuh-0002";
    public static final String ODSDATABASE="gmall";

}
