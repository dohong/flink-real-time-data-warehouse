package com.jolohong.Ware.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtil {

    private final static DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final static SimpleDateFormat simpleDateFromat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String toYMDhms(String date){
        return toYMDhms(new Date(Long.parseLong(date)));
    }

    public static String toYMDhms(Date date) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return formater.format(localDateTime);
    }


    public static Long toTs(String YmDHms) {
        LocalDateTime localDateTime = LocalDateTime.parse(YmDHms, formater);
        return localDateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    public static Long checkTs(String Ts) {
        try {
            return simpleDateFromat.parse(Ts).getTime();
        } catch (ParseException e) {
            return new Date(Long.parseLong(Ts)).getTime();
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1l;
    }
}
