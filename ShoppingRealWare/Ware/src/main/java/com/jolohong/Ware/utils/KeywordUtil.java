package com.jolohong.Ware.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class KeywordUtil {
    public static List<String> splitKeyWord(String keyWord) throws IOException {
        ArrayList<String> resultList=new ArrayList<>();
        StringReader stringReader = new StringReader(keyWord);
        IKSegmenter ikSegmenter = new IKSegmenter(stringReader, false);
        while (true){
            Lexeme next = ikSegmenter.next();
            if(next!=null){
                String word = next.getLexemeText();
                resultList.add(word);
            }else {
                break;
            }
        }
        return resultList;
    }
}
