package com.jolohong.Ware.utils;

import com.jolohong.Ware.config.Config;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {
    public static JedisPool jedisPool = null;

    public static Jedis getJedis() {
        if(jedisPool==null) {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(1000); //最大可用连接数
            jedisPoolConfig.setBlockWhenExhausted(true); //连接耗尽是否等待
            jedisPoolConfig.setMaxWaitMillis(5000); //等待时间
            jedisPoolConfig.setMaxIdle(5); //最大闲置连接数
            jedisPoolConfig.setMinIdle(5); //最小闲置连接数
            jedisPoolConfig.setTestOnBorrow(true); //获取连接的时候进行一下测试 ping pong
            jedisPool = new JedisPool(jedisPoolConfig, Config.REDISHOST, Config.REDISPOST, 6379,Config.REDISAUTH);
            return jedisPool.getResource();

        }else {
            System.out.println("redis 存活连接： " + jedisPool.getNumActive() + "redis 等待连接数" + jedisPool.getNumWaiters());
            return jedisPool.getResource();
        }
    }
}
