import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jolohong.Ware.been.OrderInfo;
import com.jolohong.Ware.been.OrderWide;
import com.jolohong.Ware.been.ProvinceStats;
import com.jolohong.Ware.config.Config;
import com.jolohong.Ware.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.table.api.*;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import static org.apache.flink.table.api.Expressions.*;


public class OrderWideTest {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME","jolohong");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setParallelism(1);
        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointTimeout(10000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        env.setStateBackend(new FsStateBackend("file:///flink/checkpoint/ck/"));

        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create( env, settings);

        TableConfig config = tableEnvironment.getConfig();
//        config.getConfiguration().setBoolean("table.exec.emit.early-fire.enabled", true);
//        config.getConfiguration().setString("table.exec.emit.early-fire.delay","5s");
        String groupId = "province_stats_test";
        String orderWideTopic = "dwm_order_wide";
//        tableEnvironment.executeSql("CREATE TABLE `order_wide` ( " +
//                "  `province_id` BIGINT, " +
//                "  `province_name` STRING, " +
//                "  `province_area_code` STRING, " +
//                "  `province_iso_code` STRING, " +
//                "  `province_3166_2_code` STRING, " +
//                "  `order_id` BIGINT, " +
//                "  `split_total_amount` DECIMAL, " +
//                "  `create_time` BIGINT, " +
//                "  `rt` as TO_TIMESTAMP(FROM_UNIXTIME(create_time/1000, 'yyyy-MM-dd HH:mm:ss')), " +
//                "  WATERMARK FOR rt AS rt - INTERVAL '1' SECOND ) with(" +
//                KafkaUtil.getKafkaDDL(orderWideTopic, groupId) + ")").print();

        Properties properties = new Properties();
        properties.setProperty("group.id", "test");
        properties.setProperty("bootstrap.servers", Config.BOOSTARTSERVERS);
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("auto.commit.interval.ms",50000);
        properties.put("auto.offset.reset", "earliest");
        FlinkKafkaConsumer<String> consumer = new FlinkKafkaConsumer<>(orderWideTopic, new SimpleStringSchema(), properties);
        consumer.setCommitOffsetsOnCheckpoints(true);
        consumer.setStartFromEarliest();
        SingleOutputStreamOperator<Province> source = env.addSource(consumer).map(line -> {OrderWide orderWide= JSON.parseObject(line, OrderWide.class);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return new Province(orderWide.getProvince_id(),
                orderWide.getProvince_name(),
                orderWide.getProvince_area_code(),
                orderWide.getProvince_iso_code(),
                orderWide.getProvince_3166_2_code(),
                orderWide.getOrder_id(),
                orderWide.getSplit_total_amount(), LocalDateTime.parse("2020-04-15 08:05", dateTimeFormatter)
                );
        });
//        source.print("----->");

        Table order_wide = tableEnvironment.fromDataStream(source, Schema.newBuilder()
                .column("province_id", DataTypes.BIGINT())
                .column("province_name", DataTypes.STRING())
                .column("province_area_code", DataTypes.STRING())
                .column("province_iso_code", DataTypes.STRING())
                .column("province_3166_2_code", DataTypes.STRING())
                .column("order_id", DataTypes.BIGINT())
                .column("split_total_amount", DataTypes.DECIMAL(16, 2))
                .column("create_time", DataTypes.TIMESTAMP(3))
                .watermark("create_time", "create_time - INTERVAL '1' SECOND")
                .build());
//        order_wide.printSchema();
//        order_wide.execute().print();
        tableEnvironment.createTemporaryView("order_wide",order_wide);
//
//
        String SQL="SELECT   " +
                "window_start,window_end,window_time,count(distinct order_id) as num  \n" +
                "FROM  \n" +
                "TABLE( TUMBLE(TABLE order_wide, DESCRIPTOR(create_time), INTERVAL '30' SECOND)) \n" +
//                "TABLE(HOP(TABLE order_wide, DESCRIPTOR(rt), INTERVAL '1' MINUTES, INTERVAL '5' MINUTES)) \n" +
//                "TABLE (CUMULATE(TABLE order_wide, DESCRIPTOR(create_time), INTERVAL '5' SECOND, INTERVAL '10' SECOND))"+
                "group by \n" +
                "window_start,window_end,window_time \n" +
                "";
        tableEnvironment.executeSql(SQL).print();


        env.execute(OrderWideTest.class.getName());

    }
    public static class Province{
        public Long province_id;
        public String province_name;
        public String province_area_code;
        public String province_iso_code;
        public String province_3166_2_code;
        public Long order_id;
        public BigDecimal split_total_amount;
        public LocalDateTime create_time;

        public Long getProvince_id() {
            return province_id;
        }

        public void setProvince_id(Long province_id) {
            this.province_id = province_id;
        }

        public String getProvince_name() {
            return province_name;
        }

        public void setProvince_name(String province_name) {
            this.province_name = province_name;
        }

        public String getProvince_area_code() {
            return province_area_code;
        }

        public void setProvince_area_code(String province_area_code) {
            this.province_area_code = province_area_code;
        }

        public String getProvince_iso_code() {
            return province_iso_code;
        }

        public void setProvince_iso_code(String province_iso_code) {
            this.province_iso_code = province_iso_code;
        }

        public String getProvince_3166_2_code() {
            return province_3166_2_code;
        }

        public void setProvince_3166_2_code(String province_3166_2_code) {
            this.province_3166_2_code = province_3166_2_code;
        }

        public Long getOrder_id() {
            return order_id;
        }

        public void setOrder_id(Long order_id) {
            this.order_id = order_id;
        }

        public BigDecimal getSplit_total_amount() {
            return split_total_amount;
        }

        public void setSplit_total_amount(BigDecimal split_total_amount) {
            this.split_total_amount = split_total_amount;
        }

        public LocalDateTime getCreate_time() {
            return create_time;
        }

        public void setCreate_time(LocalDateTime create_time) {
            this.create_time = create_time;
        }

        public Province(){}
        public Province(Long province_id,
                        String province_name,
                        String province_area_code,
                        String province_iso_code,
                        String province_3166_2_code,
                        Long order_id,
                        BigDecimal split_total_amount,
                        LocalDateTime create_time){
            this.split_total_amount=split_total_amount;
            this.create_time=create_time;
            this.order_id=order_id;
            this.province_3166_2_code=province_3166_2_code;
            this.province_area_code=province_area_code;
            this.province_iso_code=province_iso_code;
            this.province_name=province_name;
            this.province_id=province_id;
        }

        @Override
        public String toString() {
            return "Province{" +
                    "province_id=" + province_id +
                    ", province_name='" + province_name + '\'' +
                    ", province_area_code='" + province_area_code + '\'' +
                    ", province_iso_code='" + province_iso_code + '\'' +
                    ", province_3166_2_code='" + province_3166_2_code + '\'' +
                    ", order_id=" + order_id +
                    ", split_total_amount=" + split_total_amount +
                    ", create_time=" + create_time +
                    '}';
        }
    }
}
