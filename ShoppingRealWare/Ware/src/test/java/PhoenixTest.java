import java.sql.*;
public class PhoenixTest {
    public static void create(Connection connection) {
        Connection conn = connection;
        try {
            // get connection

            // check connection
            if (conn == null) {
                System.out.println("conn is null...");
                return;
            }

            // check if the table exist
            ResultSet rs = conn.getMetaData().getTables(null, null, "USER01",
                    null);
            if (rs.next()) {
                System.out.println("table user01 is exist...");
                return;
            }else {
                System.out.println(rs == null);
                System.out.println("table doing");
            }
            // create sql
            String sql = "CREATE TABLE user01 (id varchar PRIMARY KEY,INFO.name varchar ,INFO.passwd varchar)";

            PreparedStatement ps = conn.prepareStatement(sql);

            // execute
            ps.execute();
            System.out.println("create success...");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        //1.定义参数
        String driver = "org.apache.phoenix.jdbc.PhoenixDriver";
        String url = "jdbc:phoenix:master,slave001,slave002:2181";

        //2.加载驱动
        Class.forName(driver);

        //3.创建连接
        Connection connection = DriverManager.getConnection(url);
        connection.setSchema("GMALL_realtime");
        Statement stmt = connection.createStatement();

        stmt.executeUpdate("create table test (mykey integer not null primary key, mycolumn varchar)");
        stmt.executeUpdate("upsert into test values (1,'Hello')");
        stmt.executeUpdate("upsert into test values (2,'World!')");
        connection.commit();

        PreparedStatement statement = connection.prepareStatement("select * from test");
        ResultSet rset = statement.executeQuery();
        while (rset.next()) {
            System.out.println(rset.getString("mycolumn"));
        }
        statement.close();
        connection.close();



        //4.预编译SQL
//        PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE test_salt \n" +
//                "(id VARCHAR PRIMARY KEY, \n" +
//                "name VARCHAR, age INTEGER, \n" +
//                "address VARCHAR) \n" +
//                "SALT_BUCKETS = 20");

//        //5.查询获取返回值
//        ResultSet resultSet = preparedStatement.executeQuery();

        //6.打印结果
//        while (resultSet.next()) {
//            System.out.println(resultSet.getString(1) + resultSet.getString(2));
//        }

        //7.关闭资源
//        resultSet.close();
//        preparedStatement.close();
//        connection.close();
    }
}
