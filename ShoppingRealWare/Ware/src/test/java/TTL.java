import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class TTL {
    public static void main(String[] args) {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        MapStateDescriptor mapStateDescriptor = new MapStateDescriptor<>("map-state",String.class, String.class);
        StateTtlConfig ttlConfig = StateTtlConfig
                // 状态有效时间
                .newBuilder(Time.seconds(1))
                //设置状态更新类型
                .setUpdateType(StateTtlConfig.UpdateType.OnCreateAndWrite)
                // 已过期但还未被清理掉的状态如何处理
                .setStateVisibility(StateTtlConfig.StateVisibility.NeverReturnExpired)
                // 过期对象的清理策略
                .cleanupFullSnapshot()
                .build();

//        mapStateDescriptor.enableTimeToLive(ttlConfig);
        Time ttl = mapStateDescriptor.getTtlConfig().getTtl();
        System.out.println(ttl.toMilliseconds()/1000/60/60/24);
    }
}
