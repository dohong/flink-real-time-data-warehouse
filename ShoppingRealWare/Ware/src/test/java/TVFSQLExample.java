import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.*;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TVFSQLExample {
    private static final Logger LOG = LoggerFactory.getLogger(TVFSQLExample.class);

    public static void main(String[] args) {
        EnvironmentSettings settings = null;
        StreamTableEnvironment tEnv = null;
        try {

            StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

            settings = EnvironmentSettings.newInstance()
                    .useBlinkPlanner()
                    .inStreamingMode()
                    .build();
            tEnv = StreamTableEnvironment.create(env, settings);

            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            DataStream<Bid> dataStream =
                    env.fromElements(
                            new Bid(LocalDateTime.parse("2020-04-15 08:05",dateTimeFormatter), new BigDecimal("4.00"), "C"),
                            new Bid(LocalDateTime.parse("2020-04-15 08:07",dateTimeFormatter), new BigDecimal("2.00"), "A"),
                            new Bid(LocalDateTime.parse("2020-04-15 08:09",dateTimeFormatter), new BigDecimal("5.00"), "D"),
                            new Bid(LocalDateTime.parse("2020-04-15 08:11",dateTimeFormatter), new BigDecimal("3.00"), "B"),
                            new Bid(LocalDateTime.parse("2020-04-15 08:13",dateTimeFormatter), new BigDecimal("1.00"), "E"),
                            new Bid(LocalDateTime.parse("2020-04-15 08:17",dateTimeFormatter), new BigDecimal("6.00"), "F"));

            Table table = tEnv.fromDataStream(dataStream, Schema.newBuilder()
                    .column("bidtime", DataTypes.TIMESTAMP(3))
                    .column("price", DataTypes.DECIMAL(10,2))
                    .column("item", DataTypes.STRING())
                    .watermark("bidtime", "bidtime - INTERVAL '1' SECOND")
                    .build());
            table.printSchema();

            tEnv.createTemporaryView("Bid",table);

            String sql = "SELECT window_start, window_end, SUM(price) as price FROM TABLE(" +
                    "    TUMBLE(TABLE Bid, DESCRIPTOR(bidtime), INTERVAL '10' MINUTES))" +
                    "  GROUP BY window_start, window_end";
            TableResult res = tEnv.executeSql(sql);
            res.print();

            tEnv.dropTemporaryView("Bid");
        }catch (Exception e){
            LOG.error(e.getMessage(),e);
        }
    }

    //必须是静态类，且属性
    public static class Bid{
        public LocalDateTime bidtime;
        public BigDecimal price;
        public String item;

        public Bid() {}

        public Bid(LocalDateTime bidtime, BigDecimal price, String item) {
            this.bidtime = bidtime;
            this.price = price;
            this.item = item;
        }
    }
}


