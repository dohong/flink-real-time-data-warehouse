package Test;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;

public class HUtiTEST {
    public static void main(String[] args) throws Exception {
        System.setProperty("hadoop.home.dir", "C:\\hadoop-3.0.0");
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().build();
        TableEnvironment env = TableEnvironment.create(settings);
        env.getConfig().setSqlDialect(SqlDialect.DEFAULT);
        Configuration configuration=new Configuration();
        configuration.setString("fs.default-scheme", "hdfs://124.225.182.54/");
        env.getConfig().addConfiguration(configuration);
        env.getConfig().getConfiguration().setLong("web.timeout",3000000);
        env.getConfig().getConfiguration().setLong("akka.ask.timeout", 1000);
        env.executeSql("CREATE TABLE t1(" +
                "uuid VARCHAR(20)," +
                "name VARCHAR(10)," +
                "age INT," +
                "ts TIMESTAMP(3)," +
                "`partition` VARCHAR(20)) PARTITIONED BY (`partition`) " +
                "WITH ('connector' ='hudi'," +
                "'path' = '/warehouse/gmall/t1'," +
                "'write.tasks' = '1', " +
                "'compaction.tasks' = '1'," +
                " 'table.type' = 'COPY_ON_WRITE')"
        );

        //插入一条数据
        env.executeSql("INSERT INTO t1 VALUES('id1','Danny',23,TIMESTAMP '1970-01-01 00:00:01','par1')")
                .print();
        env.sqlQuery("SELECT * FROM t1")//结果①
                .execute()
                .print();

        //修改数据
        env.executeSql("INSERT INTO t1 VALUES('id1','Danny',24,TIMESTAMP '1970-01-01 00:00:01','par1')")
                .print();
        env.sqlQuery("SELECT * FROM t1")//结果②
                .execute()
                .print();


    }
}
