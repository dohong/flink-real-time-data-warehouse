package Test;

import java.sql.*;

public class PhoenixTest {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        //1.定义参数
        String driver = "org.apache.phoenix.jdbc.PhoenixDriver";
        String url = "jdbc:phoenix:master,slave001,slave002:2181";

        //2.加载驱动
        Class.forName(driver);

        //3.创建连接
        Connection connection = DriverManager.getConnection(url);
        connection.setSchema("GMALLDB");
        Statement stmt = connection.createStatement();

        ResultSet tableTypes = connection.getMetaData().getTableTypes();
        if(!tableTypes.next()) {
            stmt.executeUpdate("create table test (mykey integer not null primary key, mycolumn varchar)");
        }
        stmt.executeUpdate("upsert into test values (1,'Honge')");
        stmt.executeUpdate("upsert into test values (2,'World!')");
        connection.commit();

        PreparedStatement statement = connection.prepareStatement("select * from test");
        ResultSet rset = statement.executeQuery();
        while (rset.next()) {
            System.out.println(rset.getString("mycolumn"));
        }
        statement.close();
        connection.close();
    }
}
