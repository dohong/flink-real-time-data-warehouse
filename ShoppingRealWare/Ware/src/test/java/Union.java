import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;
import java.util.Date;

public class Union {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.setParallelism(1);
//        DataStreamSource<T> source = env.fromCollection(Arrays.asList(new T(null, 20L), new T("hong", 100L)));
//        DataStreamSource<T> source2 = env.fromCollection(Arrays.asList(new T("zhou", null), new T("hong", null)));
//        source.union(source2,source2).print("---->");
//        env.execute("o");
        long timeMillis = System.currentTimeMillis();
        System.out.println(timeMillis);
        System.out.println(new Date(timeMillis));
        System.out.println(new Date(timeMillis + 10000));

    }
    public static class T{
        public String name;
        public Long age;
        public T(String name, Long age){
            this.name=name;this.age=age;
        }

        @Override
        public String toString() {
            return "T{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
