import com.jolohong.Ware.utils.KafkaUtil;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class payWideTest {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> stringDataStreamSource = executionEnvironment.addSource(KafkaUtil.getKafkaConsumer("dwd_payment_info", "test"));
        stringDataStreamSource.print();
        executionEnvironment.execute(payWideTest.class.getName());
    }
}
