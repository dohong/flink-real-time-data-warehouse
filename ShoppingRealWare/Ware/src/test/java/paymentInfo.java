import com.alibaba.fastjson.JSONObject;
import com.jolohong.Ware.been.OrderWide;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class paymentInfo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStreamSource<String> source = env.readTextFile("C:\\Users\\jolohong\\IdeaProjects\\ShoppingRealWare\\Ware\\src\\test\\java\\source\\json.data");
        source.map(line->
        {
            OrderWide orderWide = JSONObject.parseObject(line, OrderWide.class);

            return orderWide;
        }).print("object--->");
        env.execute();

    }
}
