import com.jolohong.Ware.utils.DateTimeUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class simple {
    public static void main(String[] args) throws ParseException {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(env);
        tableEnvironment.sqlQuery("select TO_TIMESTAMP(FROM_UNIXTIME(1650578274000/1000, 'yyyy-MM-dd HH:mm:ss'))").execute().print();
    }
}
