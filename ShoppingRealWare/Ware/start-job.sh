#!bin/bash
FLINK_BIN=/opt/module/flink-1.13.6/bin
ods(){
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.ods.FlinkCDC ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
}

dwd(){
  $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwd.BaseDBApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
  $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwd.BaseLogApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
}
dwm(){
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwm.OrderWideApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwm.PaymentWideApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwm.UniqueVisitApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dwm.UserJumpDetailApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
}
dws(){
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dws.KeywordStatsApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dws.ProductStatsApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dws.ProvinceStatsSqlApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
    $FLINK_BIN/flink rum  -m yarn-cluster -p 1 -c com.jolohong.Ware.app.dws.VisitorStatsApp ./lib/Ware-1.0-SNAPSHOT-jar-with-dependencies.jar
}

case $1 in
"ods")
  ods
;;
"dwd")
  dwd
;;
"dwm")
  dwm
;;
"dws")
  dws
;;
*)
 echo "input args ERROR"
;;
esac