create table file_info(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	file_name varchar(20) DEFAULT NULL,
	file_ex_name varchar(20) DEFAULT NULL,
	file_path varchar(20) DEFAULT NULL,
	file_system varchar(20) DEFAULT NULL,
	file_statues bigint(5) DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table tag_common_task(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	task_file_id varchar(20) DEFAULT NULL,
	main_class varchar(20) DEFAULT NULL,
	update_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table tag_info(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	tag_code varchar(20) DEFAULT NULL,
	tag_name varchar(20) DEFAULT NULL,
	tag_level bigint(20) DEFAULT NULL,
	parent_tag_id bigint(20) DEFAULT NULL,
	tag_type varchar(20) DEFAULT NULL,
	tag_value_type varchar(20) DEFAULT NULL,
	tag_value_limit decimal(16,2) DEFAULT NULL,
	tag_value_step bigint(20) DEFAULT NULL,
	tag_task_id bigint(20) DEFAULT NULL,
	tag_comment varchar(2000) DEFAULT NULL,
	update_time datetime  DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table task_info(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	task_name varchar(200) DEFAULT NULL,
	task_status varchar(20) DEFAULT NULL,
	task_comment varchar(2000) DEFAULT NULL,
	task_time varchar(10) DEFAULT NULL,
	task_type varchar(20) DEFAULT NULL,
	exec_type varchar(20) DEFAULT NULL,
	main_class varchar(200) DEFAULT NULL,
	file_id bigint(20) DEFAULT NULL,
	task_args varchar(500) DEFAULT NULL,
	task_sql varchar(5000) DEFAULT NULL,
	task_exec_level bigint(20) DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table task_process(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	task_id bigint(20) DEFAULT NULL,
	task_name varchar(200) DEFAULT NULL,
	task_exec_time varchar(10) DEFAULT NULL,
	task_busi_date varchar(10) DEFAULT NULL,
	task_exec_level bigint(20) DEFAULT NULL,
	task_exec_status bigint(20) DEFAULT NULL,
	
	yarn_app_id varchar(100) DEFAULT NULL,
	batch_id varchar(100) DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	start_time datetime DEFAULT NULL,
	end_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table task_process_log(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	task_id bigint(20) DEFAULT NULL,
	task_name varchar(20) DEFAULT NULL,
	task_stage varchar(20) DEFAULT NULL,
	task_date varchar(20) DEFAULT NULL,
	task_exec_msg varchar(20) DEFAULT NULL,
	yarn_app_id varchar(20) DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table task_tag_rule(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	tag_id bigint(20) DEFAULT NULL,
	task_id bigint(20) DEFAULT NULL,
	query_value varchar(2000) DEFAULT NULL,
	sub_tag_id bigint(20) DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table user_group(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	user_group_name varchar(200) DEFAULT NULL,
	condition_json_str varchar(200) DEFAULT NULL,
	condition_comment varchar(200) DEFAULT NULL,
	user_group_num bigint(20) DEFAULT NULL,
	update_type varchar(20) DEFAULT NULL,
	user_group_comment varchar(2000) DEFAULT NULL,
	update_time datetime DEFAULT NULL,
	create_time datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

create table user_info(
	id bigint(20) NOT NULL AUTO_INCREMENT,
	username varchar(20) DEFAULT NULL,
	password varchar(20) DEFAULT NULL,
	phone varchar(20) DEFAULT NULL,
	sex varchar(4) DEFAULT NULL,
	email varchar(20) DEFAULT NULL,
	role bigint(3) DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

















