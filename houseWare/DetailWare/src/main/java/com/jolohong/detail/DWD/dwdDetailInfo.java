package com.jolohong.detail.DWD;

import com.jolohong.detail.Const.config;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class dwdDetailInfo {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME","jolohong");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironment(1);
//        env.setParallelism(1);
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(env,settings);

        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointTimeout(1000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        env.setStateBackend(new FsStateBackend("hdfs://master:8020/flink/checkpoint/ck/"));



        // ToDo 1、用户收藏事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_favor_info (\n" +
                "`id` STRING,\n" +
                "`user_id` STRING,\n" +
                "`sku_id` STRING,\n" +
                "`spu_id` STRING,\n" +
                "`is_cancel` STRING,\n" +
                "`create_time` BIGINT,\n" +
                "`cancel_time` BIGINT\n" +
                ") \n" +
                " WITH (\n" +
                " 'connector' = 'kafka',\n" +
                " 'topic' = 'dwd_favor_info',\n" +
                " 'properties.bootstrap.servers' = '"+ config.BOOTSTRAPSERVER+"',\n" +
                " 'properties.group.id' = 'BaseDetailApp',\n" +
                " 'format' = 'json',\n" +
                "'json.fail-on-missing-field' = 'false',\n"+
                "'json.ignore-parse-errors' = 'true',\n"+
                " 'scan.startup.mode' = 'earliest-offset'\n" +
                ")");

        // TODO 检查数据
        tableEnvironment.sqlQuery("select " + "id,user_id,sku_id,spu_id,is_cancel,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_time,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( cancel_time/1000, 'yyyy-MM-dd HH:mm:ss')) as cancel_time,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_favor_info \n").execute().print();

        tableEnvironment.executeSql("CREATE TABLE dwd_favor_info(\n" +
                "`id` STRING,\n" +
                "`user_id` STRING,\n" +
                "`sku_id` STRING,\n" +
                "`spu_id` STRING,\n" +
                "`is_cancel` STRING,\n" +
                "`create_times` TIMESTAMP(3),\n" +
                "`cancel_times` TIMESTAMP(3),\n" +
                "`partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_favor_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'user_id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '30',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "    'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );


        tableEnvironment.executeSql("insert into dwd_favor_info \n " +
                "select " + "id,user_id,sku_id,spu_id,is_cancel,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( cancel_time/1000, 'yyyy-MM-dd HH:mm:ss')) as cancel_times,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_favor_info \n"
        );


         // ToDo 2、购物车事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_cart_info (\n" +
                "    `id` STRING ,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `source_type` STRING ,\n" +
                "    `source_id` STRING ,\n" +
                "    `cart_price` DECIMAL(16,2) ,\n" +
                "    `is_ordered` STRING ,\n" +
                "    `create_time` BIGINT ,\n" +
                "    `operate_time` BIGINT ,\n" +
                "    `order_time` BIGINT ,\n" +
                "    `sku_num` BIGINT \n"+"" +
                ") PARTITIONED BY (`user_id`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_cart_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
        //TODO 检查数据
        tableEnvironment.sqlQuery("select id, user_id, sku_id, source_type, source_id, cart_price, is_ordered, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( operate_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as operate_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( order_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as order_times,\n"+
                "sku_num, \n"+
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n" +
                " from kafka_cart_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_cart_info(\n" +
                "    `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `source_type` STRING ,\n" +
                "    `source_id` STRING ,\n" +
                "    `cart_price` DECIMAL(16,2) ,\n" +
                "    `is_ordered` STRING ,\n" +
                "    `create_times` TIMESTAMP(3) ,\n" +
                "    `operate_times` TIMESTAMP(3) ,\n" +
                "    `order_times` TIMESTAMP(3) ,\n" +
                "    `sku_num` BIGINT, "+
                "    `partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL +"/warehouse/gmall/dwd/dwd_cart_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_cart_info \n"+
                "select id, user_id, sku_id, source_type, source_id, cart_price, is_ordered, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( operate_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as operate_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( order_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as order_times,\n"+
                "sku_num, \n"+
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n" +
                " from kafka_cart_info");

        //TODO 3、用户评论事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_comment_info (\n" +
                "    `id` STRING ,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `spu_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `appraise` STRING ,\n" +
                "    `create_time` BIGINT "+
                ") PARTITIONED BY (`create_time`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_comment_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
        //TODO 检查数据
        tableEnvironment.sqlQuery("select id, user_id, sku_id, spu_id,order_id,appraise, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_comment_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_comment_info(\n" +
                "    `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `spu_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `appraise` STRING ,\n" +
                "    `create_times` TIMESTAMP(3), "+
                "    `partition_days` STRING\n" + ")" +
                "     PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_comment_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_comment_info \n"+
                "select id, user_id, sku_id, spu_id,order_id,appraise, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_comment_info");

        //TODO 4、退单事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_order_refund_info (\n" +
                "   `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `province_id` STRING ,\n" +
                "    `refund_type` STRING ,\n" +
                "    `refund_num` BIGINT ,\n" +
                "    `refund_amount` DECIMAL(16,2) ,\n" +
                "    `refund_reason_type` STRING ,\n" +
                "    `create_time` BIGINT"+
                ") PARTITIONED BY (`create_time`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_order_refund_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
        // TODO 检查数据
        tableEnvironment.sqlQuery(" select id, user_id, order_id,sku_id,province_id,refund_type,refund_num,refund_amount,refund_reason_type, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times, \n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_order_refund_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_order_refund_info(\n" +
                "   `id` STRING PRIMARY KEY NOT ENFORCED,\n" +
                "    `user_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `province_id` STRING ,\n" +
                "    `refund_type` STRING ,\n" +
                "    `refund_num` BIGINT ,\n" +
                "    `refund_amount` DECIMAL(16,2) ,\n" +
                "    `refund_reason_type` STRING ,\n" +
                "    `create_time` TIMESTAMP(3),\n"+
                "    `partition_days` STRING\n" + ")" +
                "     PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_order_refund_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_order_refund_info \n" +
                " select id, user_id, order_id,sku_id,province_id,refund_type,refund_num,refund_amount,refund_reason_type, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times, \n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_order_refund_info");

    }
}
