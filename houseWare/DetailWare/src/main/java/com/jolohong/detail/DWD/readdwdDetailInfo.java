package com.jolohong.detail.DWD;

import com.jolohong.detail.Const.config;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class readdwdDetailInfo {
    public static void main(String[] args) {
//        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().useBlinkPlanner().build();
//        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(executionEnvironment, settings);
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);
        tableEnvironment.executeSql("CREATE TABLE dwd_favor_info(\n" +
                "                `id` STRING,\n" +
                "                `user_id` STRING,\n" +
                "                `sku_id` STRING,\n" +
                "                `spu_id` STRING,\n" +
                "                `is_cancel` STRING,\n" +
                "                `create_times` TIMESTAMP(3),\n" +
                "                `cancel_times` TIMESTAMP(3),\n" +
                "                `partition_days` STRING\n" +
                "                ) PARTITIONED BY (`partition_days`)\n" +
                "                WITH ('connector' ='hudi',\n" +
                "                    'path' = '"+ config.HDFSURL+"/warehouse/gmall/dwd/dwd_favor_info',\n" +
                "                    'table.type' = 'COPY_ON_WRITE',\n" +
                "                    'write.operation' = 'upsert',\n" +
                "                    'write.precombine.field' = 'partition_days',\n" +
                "                    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "                    'hoodie.embed.timeline.server'= 'true',\n" +
                "                    'read.streaming.check-interval'= '5',\n" +
                "                    'read.streaming.enabled'= 'false',\n" +
                "                     'write.insert.drop.duplicates' = 'true',\n" +
                "                    'compaction.tasks' = '10',\n" +
                "                    'write.tasks' = '10' \n" +
                "                )");
        tableEnvironment.sqlQuery("select user_id from dwd_favor_info \n" +
                "group by user_id").execute().print();

    }


}
