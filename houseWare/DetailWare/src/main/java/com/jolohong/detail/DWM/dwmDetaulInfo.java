package com.jolohong.detail.DWM;

import com.jolohong.detail.Const.config;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

public class dwmDetaulInfo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(env, settings);

        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointTimeout(10000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        env.setStateBackend(new FsStateBackend("hdfs://master:8020/flink/checkpoint/ck/"));
        System.setProperty("HADOOP_USER_NAME","jolohong");

        Properties properties = new Properties();

        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "BaseDetailApp");
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, config.BOOTSTRAPSERVER);
        FlinkKafkaConsumer flinkKafkaConsumer = new FlinkKafkaConsumer<String>("dwm_unique_visit",new SimpleStringSchema(), properties);
        flinkKafkaConsumer.setStartFromEarliest();
        env.addSource(flinkKafkaConsumer).print("-----------》");
        env.execute(dwmDetaulInfo.class.getName());

//        //TODO 1、订单宽表
//        tableEnvironment.executeSql("CREATE TABLE kafka_order_wide_info (\n" +
//                "    detail_id STRING,\n" +
//                "    order_id STRING,\n" +
//                "    sku_id STRING,\n" +
//                "    order_price DECIMAL(16,2),\n" +
//                "    sku_num BIGINT,\n" +
//                "    sku_name STRING,\n" +
//                "    province_id STRING,\n" +
//                "    order_status STRING,\n" +
//                "    user_id STRING,\n" +
//                "    total_amount BIGINT,\n" +
//                "    activity_reduce_amount BIGINT,\n" +
//                "    coupon_reduce_amount BIGINT,\n" +
//                "    original_total_amount BIGINT,\n" +
//                "    feight_fee DECIMAL(16,2),\n" +
//                "    split_feight_fee DECIMAL(16,2),\n" +
//                "    split_activity_amount BIGINT,\n" +
//                "    split_coupon_amount BIGINT,\n" +
//                "    split_total_amount BIGINT,\n" +
//                "    expire_time STRING,\n" +
//                "    create_time STRING,\n" +
//                "    operate_time STRING,\n" +
//                "    create_date STRING,\n" +
//                "    create_hour STRING,\n" +
//                "    province_name STRING,\n" +
//                "    province_area_code STRING,\n" +
//                "    province_iso_code STRING,\n" +
//                "    province_3166_2_code STRING,\n" +
//                "    user_age BIGINT,\n" +
//                "    user_gender STRING,\n" +
//                "    spu_id STRING,\n" +
//                "    tm_id STRING,\n" +
//                "    category3_id STRING,\n" +
//                "    spu_name STRING,\n" +
//                "    tm_name STRING,\n" +
//                "    category3_name STRING"+
//                ")"+
//                "WITH ( \n" +
//                "'connector' = 'kafka', \n" +
//                "'topic' = 'dwm_order_wide', \n" +
//                "'properties.bootstrap.servers' = '"+ config.BOOTSTRAPSERVER+"', \n" +
//                "'properties.group.id' = 'BaseDetailApp', \n" +
//                "'format' = 'json', \n" +
//                "'scan.startup.mode' = 'earliest-offset' \n" +
//                " )");
//        tableEnvironment.sqlQuery("select * from kafka_order_wide_info").execute().print();
//        tableEnvironment.executeSql("CREATE TABLE dwd_order_wide_info(\n" +
//                "    detail_id STRING,\n" +
//                "    order_id STRING,\n" +
//                "    sku_id STRING,\n" +
//                "    order_price DECIMAL(16,2),\n" +
//                "    sku_num BIGINT,\n" +
//                "    sku_name STRING,\n" +
//                "    province_id STRING,\n" +
//                "    order_status STRING,\n" +
//                "    user_id STRING,\n" +
//                "    total_amount BIGINT,\n" +
//                "    activity_reduce_amount BIGINT,\n" +
//                "    coupon_reduce_amount BIGINT,\n" +
//                "    original_total_amount BIGINT,\n" +
//                "    feight_fee DECIMAL(16,2),\n" +
//                "    split_feight_fee DECIMAL(16,2),\n" +
//                "    split_activity_amount BIGINT,\n" +
//                "    split_coupon_amount BIGINT,\n" +
//                "    split_total_amount BIGINT,\n" +
//                "    expire_time STRING,\n" +
//                "    create_times STRING,\n" +
//                "    operate_time STRING,\n" +
//                "    create_date STRING,\n" +
//                "    create_hour STRING,\n" +
//                "    province_name STRING,\n" +
//                "    province_area_code STRING,\n" +
//                "    province_iso_code STRING,\n" +
//                "    province_3166_2_code STRING,\n" +
//                "    user_age BIGINT,\n" +
//                "    user_gender STRING,\n" +
//                "    spu_id STRING,\n" +
//                "    tm_id STRING,\n" +
//                "    category3_id STRING,\n" +
//                "    spu_name STRING,\n" +
//                "    tm_name STRING,\n" +
//                "    category3_name STRING,\n"+
//                "   `partition_days` STRING\n" +
//                ")" +"PARTITIONED BY (`partition_days`)\n"+
//                "WITH ('connector' ='hudi',\n" +
//                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_order_wide_info',\n" +
//                "    'table.type' = 'MERGE_ON_READ',\n" +
//                "    'write.operation' = 'upsert',\n" +
//                "    'write.precombine.field' = 'partition_days',\n"+
//                "    'hoodie.datasource.write.recordkey.field' = 'detail_id',\n" +
//                "    'hoodie.embed.timeline.server'= 'true',\n"+
//                "    'read.streaming.check-interval'= '5',\n"+
//                "    'read.streaming.enabled'= 'true',\n"+
//                "     'write.insert.drop.duplicates' = 'true',\n"+
//                "    'compaction.tasks' = '10',\n" +
//                "    'write.tasks' = '10' \n" +
//                ")\n"
//        );
//
//        tableEnvironment.executeSql(""+
//                "insert into dwd_order_wide_info \n" +
//                "select *, \n"+
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( cast(create_time as BIGINT)/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n"+
//                "from kafka_order_wide_info");
//
//        //TODO 2、支付宽表
//        tableEnvironment.executeSql("CREATE TABLE kafka_payment_wide_info (\n" +
//                "    payment_id STRING,\n" +
//                "    subject STRING,\n" +
//                "    payment_type STRING,\n" +
//                "    payment_create_time STRING,\n" +
//                "    callback_time STRING,\n" +
//                "    detail_id STRING,\n" +
//                "    order_id STRING,\n" +
//                "    sku_id STRING,\n" +
//                "    order_price DECIMAL(16,2),\n" +
//                "    sku_num DECIMAL(16,2),\n" +
//                "    sku_name  STRING,\n" +
//                "    province_id STRING,\n" +
//                "    order_status STRING,\n" +
//                "    user_id STRING,\n" +
//                "    total_amount DECIMAL(16,2),\n" +
//                "    activity_reduce_amount DECIMAL(16,2),\n" +
//                "    coupon_reduce_amount DECIMAL(16,2),\n" +
//                "    original_total_amount DECIMAL(16,2),\n" +
//                "    feight_fee DECIMAL(16,2),\n" +
//                "    split_feight_fee DECIMAL(16,2),\n" +
//                "    split_activity_amount DECIMAL(16,2),\n" +
//                "    split_coupon_amount DECIMAL(16,2),\n" +
//                "    split_total_amount DECIMAL(16,2),\n" +
//                "    order_create_time STRING,\n" +
//                "    province_name STRING,\n" +
//                "    province_area_code STRING,\n" +
//                "    province_iso_code STRING,\n" +
//                "    province_3166_2_code STRING,\n" +
//                "    user_age BIGINT,\n" +
//                "    user_gender  STRING,\n" +
//                "    spu_id STRING,\n" +
//                "    tm_id STRING,\n" +
//                "    category3_id STRING,\n" +
//                "    spu_name STRING,\n" +
//                "    tm_name STRING,\n" +
//                "    category3_name STRING"+
//                ")" +
//                "WITH ( \n" +
//                "'connector' = 'kafka', \n" +
//                "'topic' = 'dwd_payment_wide_info_test', \n" +
//                "'properties.bootstrap.servers' = 'master:9092,slave001:9092,slave002:9092', \n" +
//                "'properties.group.id' = 'BaseDetailApp', \n" +
//                "'format' = 'json', \n" +
//                "'scan.startup.mode' = 'earliest-offset' \n" +
//                " )");
//        tableEnvironment.executeSql("CREATE TABLE dwd_payment_wide_info(\n" +
//                "    payment_id STRING ,\n" +
//                "    subject STRING,\n" +
//                "    payment_type STRING,\n" +
//                "    payment_create_time STRING,\n" +
//                "    callback_time STRING,\n" +
//                "    detail_id STRING,\n" +
//                "    order_id STRING,\n" +
//                "    sku_id STRING,\n" +
//                "    order_price DECIMAL(16,2),\n" +
//                "    sku_num DECIMAL(16,2),\n" +
//                "    sku_name STRING,\n" +
//                "    province_id STRING,\n" +
//                "    order_status STRING,\n" +
//                "    user_id STRING,\n" +
//                "    total_amount DECIMAL(16,2),\n" +
//                "    activity_reduce_amount DECIMAL(16,2),\n" +
//                "    coupon_reduce_amount DECIMAL(16,2),\n" +
//                "    original_total_amount DECIMAL(16,2),\n" +
//                "    feight_fee DECIMAL(16,2),\n" +
//                "    split_feight_fee DECIMAL(16,2),\n" +
//                "    split_activity_amount DECIMAL(16,2),\n" +
//                "    split_coupon_amount DECIMAL(16,2),\n" +
//                "    split_total_amount DECIMAL(16,2),\n" +
//                "    order_create_time STRING,\n" +
//                "    province_name STRING,\n" +
//                "    province_area_code STRING,\n" +
//                "    province_iso_code STRING,\n" +
//                "    province_3166_2_code STRING,\n" +
//                "    user_age BIGINT,\n" +
//                "    user_gender  STRING,\n" +
//                "    spu_id STRING,\n" +
//                "    tm_id STRING,\n" +
//                "    category3_id STRING,\n" +
//                "    spu_name STRING,\n" +
//                "    tm_name STRING,\n" +
//                "    category3_name STRING, \n"+
//                "   `partition_days` STRING\n" +
//                ")" +"PARTITIONED BY (`partition_days`)\n"+
//                "WITH ('connector' ='hudi',\n" +
//                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_payment_wide_info',\n" +
//                "    'table.type' = 'MERGE_ON_READ',\n" +
//                "    'write.operation' = 'upsert',\n" +
//                "    'write.precombine.field' = 'partition_days',\n"+
//                "    'hoodie.datasource.write.recordkey.field' = 'payment_id',\n" +
//                "    'hoodie.embed.timeline.server'= 'true',\n"+
//                "    'read.streaming.check-interval'= '5',\n"+
//                "    'read.streaming.enabled'= 'true',\n"+
//                "     'write.insert.drop.duplicates' = 'true',\n"+
//                "    'compaction.tasks' = '10',\n" +
//                "    'write.tasks' = '10' \n" +
//                ")\n"
//        );
//        tableEnvironment.executeSql("insert into dwd_payment_wide_info \n" +
//                "select *, \n" +
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( cast(payment_create_time as BIGINT)/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n"+
//                " from kafka_payment_wide_info");

    }
}
