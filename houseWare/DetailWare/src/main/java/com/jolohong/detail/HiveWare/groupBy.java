package com.jolohong.detail.HiveWare;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

public class groupBy {
    public static void main(String[] args) {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        environment.setParallelism(1);
        EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);
        //注册hive catalog
        String catalogName = "catalog";
        String defaultDatabase = "gmalldb";
        String HiveConfDir="C:\\Users\\jolohong\\IdeaProjects\\houseWare\\DetailWare\\src\\main\\resources";
        HiveCatalog hiveCatalog = new HiveCatalog(catalogName, defaultDatabase, HiveConfDir);
        tableEnvironment.registerCatalog(catalogName,hiveCatalog);
        tableEnvironment.useCatalog(catalogName);
        tableEnvironment.sqlQuery("select user_id,province_name,count(*) as order_count" +
                "from dwd_order_wide_info " +
                "where datestr='2022-05-12' " +
                "group by user_id,province_name ").execute().print();

    }
}
