package com.jolohong.detail;

import com.jolohong.detail.Const.config;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class collectorMain {
    public static void main(String[] args) {
        System.setProperty("HADOOP_USER_NAME","jolohong");
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        environment.setParallelism(1);
        environment.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
        environment.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        environment.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        environment.setStateBackend(new FsStateBackend(config.HDFSURL+"/flink/ck"));
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(environment, settings);

        // ToDo 1、用户收藏事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_favor_info (\n" +
                "`id` STRING,\n" +
                "`user_id` STRING,\n" +
                "`sku_id` STRING,\n" +
                "`spu_id` STRING,\n" +
                "`is_cancel` STRING,\n" +
                "`create_time` BIGINT,\n" +
                "`cancel_time` BIGINT\n" +
                ") \n" +
                " WITH (\n" +
                " 'connector' = 'kafka',\n" +
                " 'topic' = 'dwd_favor_info',\n" +
                " 'properties.bootstrap.servers' = '"+ config.BOOTSTRAPSERVER+"',\n" +
                " 'properties.group.id' = 'BaseDetailApp',\n" +
                " 'format' = 'json',\n" +
                "'json.fail-on-missing-field' = 'false',\n"+
                "'json.ignore-parse-errors' = 'true',\n"+
                " 'scan.startup.mode' = 'earliest-offset'\n" +
                ")");
//        // TODO 检查数据
//        tableEnvironment.sqlQuery("select " + "id,user_id,sku_id,spu_id,is_cancel,\n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_time,\n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( cancel_time/1000, 'yyyy-MM-dd HH:mm:ss')) as cancel_time,\n" +
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
//                "from kafka_favor_info \n").execute().print();
        tableEnvironment.executeSql("CREATE TABLE dwd_favor_info(\n" +
                "`id` STRING,\n" +
                "`user_id` STRING,\n" +
                "`sku_id` STRING,\n" +
                "`spu_id` STRING,\n" +
                "`is_cancel` STRING,\n" +
                "`create_times` TIMESTAMP(3),\n" +
                "`cancel_times` TIMESTAMP(3),\n" +
                "`partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_favor_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'user_id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '30',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "    'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );

        tableEnvironment.executeSql("insert into dwd_favor_info \n " +
                "select " + "id,user_id,sku_id,spu_id,is_cancel,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( cancel_time/1000, 'yyyy-MM-dd HH:mm:ss')) as cancel_times,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_favor_info \n"
        );

        // ToDo 2、购物车事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_cart_info (\n" +
                "    `id` STRING ,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `source_type` STRING ,\n" +
                "    `source_id` STRING ,\n" +
                "    `cart_price` DECIMAL(16,2) ,\n" +
                "    `is_ordered` STRING ,\n" +
                "    `create_time` BIGINT ,\n" +
                "    `operate_time` BIGINT ,\n" +
                "    `order_time` BIGINT ,\n" +
                "    `sku_num` BIGINT \n"+"" +
                ") PARTITIONED BY (`user_id`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_cart_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
//        //TODO 检查数据
//        tableEnvironment.sqlQuery("select id, user_id, sku_id, source_type, source_id, cart_price, is_ordered, \n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( create_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( operate_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as operate_times,\n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( order_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as order_times,\n"+
//                "sku_num, \n"+
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n" +
//                " from kafka_cart_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_cart_info(\n" +
                "    `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `source_type` STRING ,\n" +
                "    `source_id` STRING ,\n" +
                "    `cart_price` DECIMAL(16,2) ,\n" +
                "    `is_ordered` STRING ,\n" +
                "    `create_times` TIMESTAMP(3) ,\n" +
                "    `operate_times` TIMESTAMP(3) ,\n" +
                "    `order_times` TIMESTAMP(3) ,\n" +
                "    `sku_num` BIGINT, "+
                "    `partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL +"/warehouse/gmall/dwd/dwd_cart_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_cart_info \n"+
                "select id, user_id, sku_id, source_type, source_id, cart_price, is_ordered, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( operate_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as operate_times,\n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( order_time / 1000, 'yyyy-MM-dd HH:mm:ss')) as order_times,\n"+
                "sku_num, \n"+
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n" +
                " from kafka_cart_info");

        //TODO 3、用户评论事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_comment_info (\n" +
                "    `id` STRING ,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `spu_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `appraise` STRING ,\n" +
                "    `create_time` BIGINT "+
                ") PARTITIONED BY (`create_time`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_comment_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
//        //TODO 检查数据
//        tableEnvironment.sqlQuery("select id, user_id, sku_id, spu_id,order_id,appraise, \n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
//                "from kafka_comment_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_comment_info(\n" +
                "    `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `spu_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `appraise` STRING ,\n" +
                "    `create_times` TIMESTAMP(3), "+
                "    `partition_days` STRING\n" + ")" +
                "     PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_comment_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_comment_info \n"+
                "select id, user_id, sku_id, spu_id,order_id,appraise, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_comment_info");

        //TODO 4、退单事实表
        tableEnvironment.executeSql("CREATE TABLE kafka_order_refund_info (\n" +
                "   `id` STRING,\n" +
                "    `user_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `province_id` STRING ,\n" +
                "    `refund_type` STRING ,\n" +
                "    `refund_num` BIGINT ,\n" +
                "    `refund_amount` DECIMAL(16,2) ,\n" +
                "    `refund_reason_type` STRING ,\n" +
                "    `create_time` BIGINT"+
                ") PARTITIONED BY (`create_time`)  \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwd_order_refund_info', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
//        // TODO 检查数据
//        tableEnvironment.sqlQuery(" select id, user_id, order_id,sku_id,province_id,refund_type,refund_num,refund_amount,refund_reason_type, \n" +
//                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times, \n" +
//                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
//                "from kafka_order_refund_info").execute().print();


        tableEnvironment.executeSql("CREATE TABLE dwd_order_refund_info(\n" +
                "   `id` STRING PRIMARY KEY NOT ENFORCED,\n" +
                "    `user_id` STRING ,\n" +
                "    `order_id` STRING ,\n" +
                "    `sku_id` STRING ,\n" +
                "    `province_id` STRING ,\n" +
                "    `refund_type` STRING ,\n" +
                "    `refund_num` BIGINT ,\n" +
                "    `refund_amount` DECIMAL(16,2) ,\n" +
                "    `refund_reason_type` STRING ,\n" +
                "    `create_time` TIMESTAMP(3),\n"+
                "    `partition_days` STRING\n" + ")" +
                "     PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_order_refund_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_order_refund_info \n" +
                " select id, user_id, order_id,sku_id,province_id,refund_type,refund_num,refund_amount,refund_reason_type, \n" +
                "TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')) as create_times, \n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( create_time/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_order_refund_info");

        //TODO 1、订单宽表
        tableEnvironment.executeSql("CREATE TABLE kafka_order_wide_info (\n" +
                "    detail_id STRING,\n" +
                "    order_id STRING,\n" +
                "    sku_id STRING,\n" +
                "    order_price DECIMAL(16,2),\n" +
                "    sku_num BIGINT,\n" +
                "    sku_name STRING,\n" +
                "    province_id STRING,\n" +
                "    order_status STRING,\n" +
                "    user_id STRING,\n" +
                "    total_amount BIGINT,\n" +
                "    activity_reduce_amount BIGINT,\n" +
                "    coupon_reduce_amount BIGINT,\n" +
                "    original_total_amount BIGINT,\n" +
                "    feight_fee DECIMAL(16,2),\n" +
                "    split_feight_fee DECIMAL(16,2),\n" +
                "    split_activity_amount BIGINT,\n" +
                "    split_coupon_amount BIGINT,\n" +
                "    split_total_amount BIGINT,\n" +
                "    expire_time STRING,\n" +
                "    create_time STRING,\n" +
                "    operate_time STRING,\n" +
                "    create_date STRING,\n" +
                "    create_hour STRING,\n" +
                "    province_name STRING,\n" +
                "    province_area_code STRING,\n" +
                "    province_iso_code STRING,\n" +
                "    province_3166_2_code STRING,\n" +
                "    user_age BIGINT,\n" +
                "    user_gender STRING,\n" +
                "    spu_id STRING,\n" +
                "    tm_id STRING,\n" +
                "    category3_id STRING,\n" +
                "    spu_name STRING,\n" +
                "    tm_name STRING,\n" +
                "    category3_name STRING"+
                ")"+
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwm_order_wide', \n" +
                "'properties.bootstrap.servers' = '"+ config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");


        tableEnvironment.executeSql("CREATE TABLE dwd_order_wide_info(\n" +
                "    detail_id STRING,\n" +
                "    order_id STRING,\n" +
                "    sku_id STRING,\n" +
                "    order_price DECIMAL(16,2),\n" +
                "    sku_num BIGINT,\n" +
                "    sku_name STRING,\n" +
                "    province_id STRING,\n" +
                "    order_status STRING,\n" +
                "    user_id STRING,\n" +
                "    total_amount BIGINT,\n" +
                "    activity_reduce_amount BIGINT,\n" +
                "    coupon_reduce_amount BIGINT,\n" +
                "    original_total_amount BIGINT,\n" +
                "    feight_fee DECIMAL(16,2),\n" +
                "    split_feight_fee DECIMAL(16,2),\n" +
                "    split_activity_amount BIGINT,\n" +
                "    split_coupon_amount BIGINT,\n" +
                "    split_total_amount BIGINT,\n" +
                "    expire_time STRING,\n" +
                "    create_times STRING,\n" +
                "    operate_time STRING,\n" +
                "    create_date STRING,\n" +
                "    create_hour STRING,\n" +
                "    province_name STRING,\n" +
                "    province_area_code STRING,\n" +
                "    province_iso_code STRING,\n" +
                "    province_3166_2_code STRING,\n" +
                "    user_age BIGINT,\n" +
                "    user_gender STRING,\n" +
                "    spu_id STRING,\n" +
                "    tm_id STRING,\n" +
                "    category3_id STRING,\n" +
                "    spu_name STRING,\n" +
                "    tm_name STRING,\n" +
                "    category3_name STRING,\n"+
                "   `partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_order_wide_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'detail_id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );

        tableEnvironment.executeSql(""+
                "insert into dwd_order_wide_info \n" +
                "select *, \n"+
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( cast(create_time as BIGINT)/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n"+
                "from kafka_order_wide_info");

        //TODO 2、支付宽表
        tableEnvironment.executeSql("CREATE TABLE kafka_payment_wide_info (\n" +
                "    payment_id STRING,\n" +
                "    subject STRING,\n" +
                "    payment_type STRING,\n" +
                "    payment_create_time STRING,\n" +
                "    callback_time STRING,\n" +
                "    detail_id STRING,\n" +
                "    order_id STRING,\n" +
                "    sku_id STRING,\n" +
                "    order_price DECIMAL(16,2),\n" +
                "    sku_num DECIMAL(16,2),\n" +
                "    sku_name  STRING,\n" +
                "    province_id STRING,\n" +
                "    order_status STRING,\n" +
                "    user_id STRING,\n" +
                "    total_amount DECIMAL(16,2),\n" +
                "    activity_reduce_amount DECIMAL(16,2),\n" +
                "    coupon_reduce_amount DECIMAL(16,2),\n" +
                "    original_total_amount DECIMAL(16,2),\n" +
                "    feight_fee DECIMAL(16,2),\n" +
                "    split_feight_fee DECIMAL(16,2),\n" +
                "    split_activity_amount DECIMAL(16,2),\n" +
                "    split_coupon_amount DECIMAL(16,2),\n" +
                "    split_total_amount DECIMAL(16,2),\n" +
                "    order_create_time STRING,\n" +
                "    province_name STRING,\n" +
                "    province_area_code STRING,\n" +
                "    province_iso_code STRING,\n" +
                "    province_3166_2_code STRING,\n" +
                "    user_age BIGINT,\n" +
                "    user_gender  STRING,\n" +
                "    spu_id STRING,\n" +
                "    tm_id STRING,\n" +
                "    category3_id STRING,\n" +
                "    spu_name STRING,\n" +
                "    tm_name STRING,\n" +
                "    category3_name STRING"+
                ")" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwm_payment_wide', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailApp', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )");
        tableEnvironment.executeSql("CREATE TABLE dwd_payment_wide_info(\n" +
                "    payment_id STRING ,\n" +
                "    subject STRING,\n" +
                "    payment_type STRING,\n" +
                "    payment_create_time STRING,\n" +
                "    callback_time STRING,\n" +
                "    detail_id STRING,\n" +
                "    order_id STRING,\n" +
                "    sku_id STRING,\n" +
                "    order_price DECIMAL(16,2),\n" +
                "    sku_num DECIMAL(16,2),\n" +
                "    sku_name STRING,\n" +
                "    province_id STRING,\n" +
                "    order_status STRING,\n" +
                "    user_id STRING,\n" +
                "    total_amount DECIMAL(16,2),\n" +
                "    activity_reduce_amount DECIMAL(16,2),\n" +
                "    coupon_reduce_amount DECIMAL(16,2),\n" +
                "    original_total_amount DECIMAL(16,2),\n" +
                "    feight_fee DECIMAL(16,2),\n" +
                "    split_feight_fee DECIMAL(16,2),\n" +
                "    split_activity_amount DECIMAL(16,2),\n" +
                "    split_coupon_amount DECIMAL(16,2),\n" +
                "    split_total_amount DECIMAL(16,2),\n" +
                "    order_create_time STRING,\n" +
                "    province_name STRING,\n" +
                "    province_area_code STRING,\n" +
                "    province_iso_code STRING,\n" +
                "    province_3166_2_code STRING,\n" +
                "    user_age BIGINT,\n" +
                "    user_gender  STRING,\n" +
                "    spu_id STRING,\n" +
                "    tm_id STRING,\n" +
                "    category3_id STRING,\n" +
                "    spu_name STRING,\n" +
                "    tm_name STRING,\n" +
                "    category3_name STRING, \n"+
                "   `partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_payment_wide_info',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'payment_id',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '5',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "     'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_payment_wide_info \n" +
                "select *, \n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( cast(payment_create_time as BIGINT)/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days \n"+
                " from kafka_payment_wide_info");

        //TODO 用户访问明细
        tableEnvironment.executeSql("CREATE TABLE kafka_unique_visit (\n" +
                "common ROW(ar STRING,uid STRING,os STRING, ch STRING,is_new STRING, md STRING, mid STRING, vc STRING, ba STRING), " +
                "page ROW(page_id STRING, during_time STRING),\n" +
                "err ROW(msg STRING, error_code STRING), \n" +
                "ts BIGINT \n" +
                ") \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwm_unique_visit', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailAppio', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )"
        );
        tableEnvironment.executeSql("CREATE TABLE dwd_unique_visit(\n" +
                "`ar` STRING,\n" +
                "`uid` STRING,\n" +
                "`os` STRING,\n" +
                "`ch` STRING,\n" +
                "`is_new` STRING,\n" +
                "`md` STRING,\n" +
                "`mid` STRING,\n" +
                "`vc` STRING, \n" +
                "`ba` STRING, \n" +
                "`page_id` STRING, \n" +
                "during_time STRING, \n" +
                "`msg` STRING,\n" +
                "`error_code` STRING,\n" +
                "`ts` STRING, \n" +
                "`partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_unique_visit',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'uid',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '30',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "    'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_unique_visit select  \n" +
                "common['ar'] as ar,common['uid'] as uid, \n" +
                "common['os'] as os, common['ch'] as ch,common['is_new'] as is_new,common['md'] as md, \n" +
                "common['mid'] as mid, common['vc'] as vc, common['ba'] as ba, \n" +
                "page['page_id'] as page_id, page['during_time'] as during_time,\n" +
                "err['msg'] as msg, err['error_code'] as error_code,\n" +
                "cast(TO_TIMESTAMP(FROM_UNIXTIME(ts / 1000, 'yyyy-MM-dd HH:mm:ss')) as STRING) as ts,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( ts/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_unique_visit \n");

        // TODO 用户跳出明细
        tableEnvironment.executeSql("CREATE TABLE kafka_user_jump_detail (\n" +
                "common ROW(ar STRING,uid STRING,os STRING, ch STRING,is_new STRING, md STRING, mid STRING, vc STRING, ba STRING), " +
                "page ROW(page_id STRING, during_time STRING),\n" +
                "err ROW(msg STRING, error_code STRING), \n" +
                "ts BIGINT \n" +
                ") \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwm_user_jump_detail', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailAppio', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )"
        );
        tableEnvironment.executeSql("CREATE TABLE dwd_user_jump_detail(\n" +
                "`ar` STRING,\n" +
                "`uid` STRING,\n" +
                "`os` STRING,\n" +
                "`ch` STRING,\n" +
                "`is_new` STRING,\n" +
                "`md` STRING,\n" +
                "`mid` STRING,\n" +
                "`vc` STRING, \n" +
                "`ba` STRING, \n" +
                "`page_id` STRING, \n" +
                "during_time STRING, \n" +
                "`msg` STRING,\n" +
                "`error_code` STRING,\n" +
                "`ts` STRING, \n" +
                "`partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwd/dwd_user_jump_detail',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'uid',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '30',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "    'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_user_jump_detail select  \n" +
                "common['ar'] as ar,common['uid'] as uid, \n" +
                "common['os'] as os, common['ch'] as ch,common['is_new'] as is_new,common['md'] as md, \n" +
                "common['mid'] as mid, common['vc'] as vc, common['ba'] as ba, \n" +
                "page['page_id'] as page_id, page['during_time'] as during_time,\n" +
                "err['msg'] as msg, err['error_code'] as error_code,\n" +
                "cast(TO_TIMESTAMP(FROM_UNIXTIME(ts / 1000, 'yyyy-MM-dd HH:mm:ss')) as STRING) as ts,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( ts/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_user_jump_detail \n");


    }
}
