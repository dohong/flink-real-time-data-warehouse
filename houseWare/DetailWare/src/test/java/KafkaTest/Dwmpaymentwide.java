package KafkaTest;

import com.jolohong.detail.Const.config;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

public class Dwmpaymentwide {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        environment.setParallelism(1);
        environment.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
        environment.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        environment.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        Properties properties = new Properties();
//        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "BaseDetailApp001");
//        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, config.BOOTSTRAPSERVER);
//        FlinkKafkaConsumer flinkKafkaConsumer = new FlinkKafkaConsumer<String>("dwm_user_jump_detail",new SimpleStringSchema(), properties);
//        flinkKafkaConsumer.setStartFromEarliest();
//        DataStreamSource dataStreamSource = environment.addSource(flinkKafkaConsumer);
//        dataStreamSource.print();

        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(environment, settings);
        tableEnvironment.executeSql("CREATE TABLE kafka_unique_visit (\n" +
                "common ROW(ar STRING,uid STRING,os STRING, ch STRING,is_new STRING, md STRING, mid STRING, vc STRING, ba STRING), " +
                "page ROW(page_id STRING, during_time STRING),\n" +
                "err ROW(msg STRING, error_code STRING), \n" +
                "ts BIGINT \n" +
                ") \n" +
                "WITH ( \n" +
                "'connector' = 'kafka', \n" +
                "'topic' = 'dwm_unique_visit', \n" +
                "'properties.bootstrap.servers' = '"+config.BOOTSTRAPSERVER+"', \n" +
                "'properties.group.id' = 'BaseDetailAppio', \n" +
                "'format' = 'json', \n" +
                "'scan.startup.mode' = 'earliest-offset' \n" +
                " )"
        );
        tableEnvironment.executeSql("CREATE TABLE dwd_unique_visit(\n" +
                "`ar` STRING,\n" +
                "`uid` STRING,\n" +
                "`os` STRING,\n" +
                "`ch` STRING,\n" +
                "`is_new` STRING,\n" +
                "`md` STRING,\n" +
                "`mid` STRING,\n" +
                "`vc` STRING, \n" +
                "`ba` STRING, \n" +
                "`page_id` STRING, \n" +
                "during_time STRING, \n" +
                "`msg` STRING,\n" +
                "`error_code` STRING,\n" +
                "`ts` STRING, \n" +
                "`partition_days` STRING\n" +
                ")" +"PARTITIONED BY (`partition_days`)\n"+
                "WITH ('connector' ='hudi',\n" +
                "    'path' = '"+config.HDFSURL+"/warehouse/gmall/dwm/dwd_unique_visit',\n" +
                "    'table.type' = 'MERGE_ON_READ',\n" +
                "    'write.operation' = 'upsert',\n" +
                "    'write.precombine.field' = 'partition_days',\n"+
                "    'hoodie.datasource.write.recordkey.field' = 'uid',\n" +
                "    'hoodie.embed.timeline.server'= 'true',\n"+
                "    'read.streaming.check-interval'= '30',\n"+
                "    'read.streaming.enabled'= 'true',\n"+
                "    'write.insert.drop.duplicates' = 'true',\n"+
                "    'compaction.tasks' = '10',\n" +
                "    'write.tasks' = '10' \n" +
                ")\n"
        );
        tableEnvironment.executeSql("insert into dwd_unique_visit select  \n" +
                "common['ar'] as ar,common['uid'] as uid, \n" +
                "common['os'] as os, common['ch'] as ch,common['is_new'] as is_new,common['md'] as md, \n" +
                "common['mid'] as mid, common['vc'] as vc, common['ba'] as ba, \n" +
                "page['page_id'] as page_id, page['during_time'] as during_time,\n" +
                "err['msg'] as msg, err['error_code'] as error_code,\n" +
                "cast(TO_TIMESTAMP(FROM_UNIXTIME(ts / 1000, 'yyyy-MM-dd HH:mm:ss')) as STRING) as ts,\n" +
                "DATE_FORMAT(TO_TIMESTAMP(FROM_UNIXTIME( ts/1000, 'yyyy-MM-dd HH:mm:ss')),'YYYY-MM-dd') as partition_days\n" +
                "from kafka_unique_visit \n");


    }
}
