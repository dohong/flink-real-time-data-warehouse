import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class OrderWide {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create( env, settings);

        env.enableCheckpointing(5000L);
        env.getCheckpointConfig().setCheckpointTimeout(10000L);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,5000L));
//        env.setStateBackend(new FsStateBackend("hdfs://ufbvunjvxypwdpuh-0001:8020/flink/checkpoint/ck/"));
        System.setProperty("HADOOP_USER_NAME","jolohong");
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.DEFAULT);

        //TODO 1、订单宽表
        tableEnvironment.executeSql(
                "CREATE TABLE `order_wide` ( " +
                        "activity_reduce_amount DECIMAL(16,2),\n" +
                        "category3_id BIGINT,\n" +
                        "category3_name STRING,\n" +
                        "coupon_reduce_amount DECIMAL(16,2),\n" +
                        "create_date STRING,\n" +
                        "create_hour STRING,\n" +
                        "create_time STRING,\n" +
                        "detail_id BIGINT,\n" +
                        "feight_fee DECIMAL(16,2),\n" +
                        "order_id BIGINT,\n" +
                        "order_price DECIMAL(16,2),\n" +
                        "order_status BIGINT,\n" +
                        "original_total_amount DECIMAL(16,2),\n" +
                        "province_3166_2_code STRING,\n" +
                        "province_area_code BIGINT,\n" +
                        "province_id BIGINT,\n" +
                        "province_iso_code STRING,\n" +
                        "province_name STRING,\n" +
                        "sku_id BIGINT,\n" +
                        "sku_name STRING,\n" +
                        "sku_num BIGINT,\n" +
                        "split_total_amount DECIMAL(16,2),\n" +
                        "spu_id BIGINT,\n" +
                        "spu_name STRING,\n" +
                        "tm_id BIGINT,\n" +
                        "total_amount DECIMAL(16,2),\n" +
                        "user_age BIGINT,\n" +
                        "user_gender STRING,\n" +
                        "user_id STRING\n"+
//                        "  `rt` as TO_TIMESTAMP(create_time), " +
//                        "  WATERMARK FOR rt AS rt - INTERVAL '1' SECOND
                        ")"+
                        "WITH ( \n" +
                        "'connector' = 'kafka', \n" +
                        "'topic' = 'dwm_order_wide', \n" +
                        "'properties.bootstrap.servers' = 'ufbvunjvxypwdpuh:9092,ufbvunjvxypwdpuh-0002:9092,ufbvunjvxypwdpuh-0001:9092', \n" +
                        "'properties.group.id' = 'Test', \n" +
                        "'format' = 'json', \n" +
                        "'scan.startup.mode' = 'earliest-offset' \n" +
                        " )");
        System.out.println(tableEnvironment.getCurrentDatabase());
        System.out.println(tableEnvironment.getCurrentCatalog());
        tableEnvironment.sqlQuery("select * from order_wide").execute().print();
    }
}

