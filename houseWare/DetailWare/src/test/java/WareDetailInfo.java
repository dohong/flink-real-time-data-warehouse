import org.apache.flink.configuration.Configuration;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;

public class WareDetailInfo {
    public static void main(String[] args) {
//        System.setProperty("hadoop.home.dir", "C:\\hadoop-3.0.0");
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().useBlinkPlanner().build();
        TableEnvironment env = TableEnvironment.create(settings);
//        env.getConfig().setSqlDialect(SqlDialect.DEFAULT);
//        env.getConfig().getConfiguration().setLong("web.timeout",3000000);
//        env.getConfig().getConfiguration().setLong("akka.ask.timeout", 10000);
        // todo 查询数据
//        env.executeSql("CREATE TABLE t1(" +
//                "uuid VARCHAR(20)," +
//                "name VARCHAR(10)," +
//                "age INT," +
//                "ts TIMESTAMP(3)," +
//                "`partition` VARCHAR(20)) PARTITIONED BY (`partition`) " +
//                "WITH ('connector' ='hudi'," +
//                "'path' = 'hdfs://master:8020/gmall/t1'," +
////                "'write.tasks' = '1', " +
////                "'compaction.tasks' = '1'," +
//                "'read.streaming.enabled'= 'false', "+
////                "'hoodie.datasource.write.partitionpath.field' = 'ts',"+
////                "'hoodie.embed.timeline.server'= 'false',"+
//                "'read.streaming.check-interval'= '4',"+
////                " 'table.type' = 'COPY_ON_WRITE'" +
//                "'table.type'='MERGE_ON_READ'"+
//                ")"
//        );



        // todo 插入数据
        env.executeSql("CREATE TABLE t1(" +
                        "uuid VARCHAR(20)," +
                        "name VARCHAR(10)," +
                        "age INT," +
                        "ts TIMESTAMP(3)," +
                        "`partition` VARCHAR(20)) PARTITIONED BY (`partition`) " +
                        "WITH ('connector' ='hudi'," +
                        "'path' = 'hdfs://master:8020/gmall/t1'," +
                "'write.tasks' = '1', " +
                "'compaction.tasks' = '1'," +
//                        "'read.streaming.enabled'= 'false', "+
                "'hoodie.datasource.write.partitionpath.field' = 'ts',"+
                "'hoodie.embed.timeline.server'= 'false',"+
                "'read.streaming.check-interval'= '4',"+
                " 'table.type' = 'COPY_ON_WRITE'," + ")"
        );

        env.sqlQuery("select * from t1 \n" +
                "where age>10 \n").execute().print();
        //插入一条数据
        env.executeSql("INSERT INTO t1 VALUES('id18','Danny',23,TIMESTAMP '1979-01-01 00:00:01','par27')")
                .print();
//        env.sqlQuery("SELECT * FROM t1")//结果①
//                .execute()
//                .print();

        //修改数据
//        env.executeSql("INSERT INTO t1 VALUES('id1','Danny',24,TIMESTAMP '1970-01-01 00:00:01','par1')")
//                .print();
//        env.sqlQuery("SELECT * FROM t1")//结果②
//                .execute()
//                .print();



    }
}
