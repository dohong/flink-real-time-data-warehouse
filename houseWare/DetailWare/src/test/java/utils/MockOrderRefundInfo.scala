package utils

import org.apache.commons.lang3.time.FastDateFormat
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer
import org.json4s.jackson.Json
import utils.MockFavorIfo.getDate

import java.util.Properties
import scala.util.Random

case class OrderRefundInfo(
                               id:String,
                               user_id:String,
                               order_id:String,
                               sku_id:String,
                               province_id:String,
                               refund_type:String,
                               refund_num:Int,
                               refund_amount:Double,
                               refund_reason_type:String,
                               create_time:String
)
object MockOrderRefundInfo {
  def main(args: Array[String]): Unit = {
    var producer: KafkaProducer[String, String] = null
    try {
      // 1. Kafka Client Producer 配置信息
      val props = new Properties()
      props.put("bootstrap.servers", "master:9092,slave001:9092,slave002:9092")
      props.put("acks", "1")
      props.put("retries", "3")

      //      props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      //      props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      props.put("key.serializer", classOf[StringSerializer].getName)
      props.put("value.serializer", classOf[StringSerializer].getName)

      // 2. 创建KafkaProducer对象，传入配置信息
      producer = new KafkaProducer[String, String](props)
      val random: Random = new Random()

      while (true) {
        // 每次循环 模拟产生的订单数目
        val batchNumber: Int = random.nextInt(1) + 1
        (1 to batchNumber).foreach { number =>
          val currentTime: Long = System.currentTimeMillis()
          val id: String = s"${getDate(currentTime)}%06d".format(number)
          val user_id: String = s"${1 + random.nextInt(5)}%08d".format(random.nextInt(1000))
          val create_time: String = currentTime.toString
          // 3. 订单记录数据
          val info: OrderRefundInfo = OrderRefundInfo(id, user_id, id,"1", "1", "2", 2, 300.8, "无法使用", create_time);
           //转换为JSON格式数据
          val orderJson = new Json(org.json4s.DefaultFormats).write(info)
          println(orderJson)
          // 4. 构建ProducerRecord对象
          val record = new ProducerRecord[String, String]("dwd_order_refund_info_test", id, orderJson)
          // 5. 发送数据：def send(messages: KeyedMessage[K,V]*), 将数据发送到Topic
          producer.send(record)
        }
        Thread.sleep(random.nextInt(500) + 5000)
      }

    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      if (null != producer) producer.close()
    }

  }
  def getDate(time: Long, format: String = "yyyy-MM-dd HH:mm:ss:SSS"): String = {
    val fastFormat: FastDateFormat = FastDateFormat.getInstance(format)
    val formatDate: String = fastFormat.format(time) // 格式化日期
    formatDate
  }
}
