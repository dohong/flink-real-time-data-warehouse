package utils.dwmlog

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer
import org.json4s.jackson.Json
import utils.MockCartInfo.getDate

import java.util.Properties
import scala.util.Random

case class OrderWide(
                      detail_id:String,
                      order_id:String,
                      sku_id:String,
                      order_price:Double,
                      sku_num:Long,
                      sku_name:String,
                      province_id:String,
                      order_status:String,
                      user_id:String,
                      total_amount:Int,
                      activity_reduce_amount:Long,
                      coupon_reduce_amount:Long,
                      original_total_amount:Long,
                      feight_fee:Double,
                      split_feight_fee:Double,
                      split_activity_amount:Long,
                      split_coupon_amount:Long,
                      split_total_amount:Long,
                      expire_time:String,
                      create_time:String,
                      operate_time:String,
                      create_date:String,
                      create_hour:String,
                      province_name:String,
                      province_area_code:String,
                      province_iso_code:String,
                      province_3166_2_code:String,
                      user_age:Long,
                      user_gender:String,
                      spu_id:String,
                      tm_id:String,
                      category3_id:String,
                      spu_name:String,
                      tm_name:String,
                      category3_name:String
                    )

object OrderWideInfo {
  def main(args: Array[String]): Unit = {
    var producer: KafkaProducer[String, String] = null
    try {
      val properties: Properties = new Properties()
      properties.put("bootstrap.servers", "master:9092slave001:9092,slave002:9092")
      properties.put("acks", "1")
      properties.put("retries", "3")

      //      props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      //      props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      properties.put("key.serializer", classOf[StringSerializer].getName)
      properties.put("value.serializer", classOf[StringSerializer].getName)
      val producer: KafkaProducer[String, String] = new KafkaProducer[String, String](properties)
      val random: Random = new Random()
      while (true) {
        // 每次循环 模拟产生的订单数目
        val batchNumber: Int = random.nextInt(1) + 1
        (1 to batchNumber).foreach { number =>
          val currentTime: Long = System.currentTimeMillis()
          val id: String = s"${getDate(currentTime)}%06d".format(number)+"支付id"
          val user_id: String = s"${1 + random.nextInt(5)}%08d".format(random.nextInt(1000))
          val create_time:String=currentTime.toString
          val orderWide = OrderWide(id, id, id, 100.0, 100, "xiaomi", "广西", "status", id, 100, 1000, 1000, 1000, 109.89, 78.2, 100, 23, 1212, create_time,
            create_time, create_time, create_time, "12", "guangxi", "9090", "8989", "18989", 23, "M", id, id, id, "xiaomi", "yu", "hu")
          // 转换为JSON格式数据
          val orderJson = new Json(org.json4s.DefaultFormats).write(orderWide)
          println(orderJson)
          // 4. 构建ProducerRecord对象
          val record = new ProducerRecord[String, String]("dwd_order_wide_info_test", id, orderJson)
          // 5. 发送数据：def send(messages: KeyedMessage[K,V]*), 将数据发送到Topic
          producer.send(record)
        }
        Thread.sleep(random.nextInt(500) + 5000)
      }
    }catch {
      case e:Exception=>e.printStackTrace();
    }finally {
      if(producer!=null){producer.close()}
    }

  }

}
