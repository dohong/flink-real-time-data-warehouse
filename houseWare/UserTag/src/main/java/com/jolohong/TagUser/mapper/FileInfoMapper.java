package com.jolohong.TagUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.FileInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
