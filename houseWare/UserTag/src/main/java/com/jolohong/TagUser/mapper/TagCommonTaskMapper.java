package com.jolohong.TagUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.TagCommonTask;

public interface TagCommonTaskMapper extends BaseMapper<TagCommonTask> {
}
