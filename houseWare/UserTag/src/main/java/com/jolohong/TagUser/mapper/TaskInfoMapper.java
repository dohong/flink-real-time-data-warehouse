package com.jolohong.TagUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.TaskInfo;

public interface TaskInfoMapper extends BaseMapper<TaskInfo> {
}
