package com.jolohong.TagUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.TaskProcessLog;

public interface TaskProcessLogMapper extends BaseMapper<TaskProcessLog> {
}
