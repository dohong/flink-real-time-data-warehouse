package com.jolohong.TagUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.TaskTagRule;

public interface TaskTagRuleMapper extends BaseMapper<TaskTagRule> {
}
