package com.jolohong.TagUser.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TagUser.bean.UserGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@DS("mysql")
public interface UserGroupMapper extends BaseMapper<UserGroup> {
    @Insert("${sql}")
    @DS("clickhouse")
    void insertUidsSQL(@Param("sql") String sql);

    @DS("clickhouse")
    @Select("select  arrayJoin(bitmapToArray(us)) us   from user_group where user_group_id=#{id} ")
    List<String> getUserIdListByUserGruopId(@Param("id") String id);

    @DS(("clickhouse"))
    @Select("select bitmapCardinality(us) us_ct  from user_group where user_group_id =#{id}")
    Long getUserCountByUserGruopId(@Param("id") String id);


    @DS(("clickhouse"))
    @Select("${sql}")
    Long getUserCountBySQL(@Param("sql") String sql);


    @DS("clickhouse")
    @Delete("alter table  user_group  delete where user_group_id =#{id}")
    Long deleteUserCountById(@Param("id") String id);

    @DS("clickhouse")
    @Select("select count(*) from user_tag_merge_#{tagDate}")
    Long getUserCount(@Param("tagDate") String tagDate);

    @DS("mysql")
    @Select("select * from user_group where id=#{id} ")
    UserGroup getUserGroupbyid(@Param("id") String id);
}
