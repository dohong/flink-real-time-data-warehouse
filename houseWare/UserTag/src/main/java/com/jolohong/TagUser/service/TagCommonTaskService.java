package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TagCommonTask;


public interface TagCommonTaskService extends IService<TagCommonTask> {
    TagCommonTask getTagCommonTaskWithJarFile(Long id);
}
