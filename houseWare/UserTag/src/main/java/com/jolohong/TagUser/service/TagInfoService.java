package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TagInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface TagInfoService extends IService<TagInfo> {
    List<TagInfo> getTagInfoAllWithStatus();

    TagInfo getTagInfo(Long taskId);

    List<TagInfo> getTagValueList(String parentTagCode);

    Map<String,TagInfo> getTagInfoMapWithCode();

    boolean removeById(Serializable id);
}
