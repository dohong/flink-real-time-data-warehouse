package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TaskInfo;

public interface TaskInfoService extends IService<TaskInfo> {
    void saveTaskInfoWithTag(TaskInfo taskInfo);

    TaskInfo getTaskInfoWithTag(Long taskId);
}
