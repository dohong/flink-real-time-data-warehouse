package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TaskProcessLog;

public interface TaskProcessLogService extends IService<TaskProcessLog> {
}
