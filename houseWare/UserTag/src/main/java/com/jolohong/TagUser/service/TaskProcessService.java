package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TaskProcess;

import java.util.List;

public interface TaskProcessService extends IService<TaskProcess> {
    void updateStatus(Long taskProcessId, String status);

    void updateStatus(Long taskProcessId, String yarnAppId, String status);

    void genTaskProcess(String taskDate);

    List<TaskProcess> getTodoTaskProcessList(String taskTime);
}
