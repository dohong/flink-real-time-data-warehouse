package com.jolohong.TagUser.service;

import com.jolohong.TagUser.bean.TaskProcess;

public interface TaskSubmitService {
    void submitTask(TaskProcess taskProcess, boolean isRetry);
}
