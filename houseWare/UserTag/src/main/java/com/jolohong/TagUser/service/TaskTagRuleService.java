package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.TaskTagRule;

public interface TaskTagRuleService extends IService<TaskTagRule> {
}
