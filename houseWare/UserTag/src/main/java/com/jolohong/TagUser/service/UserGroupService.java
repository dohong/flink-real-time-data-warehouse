package com.jolohong.TagUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TagUser.bean.UserGroup;

public interface UserGroupService extends IService<UserGroup> {
    void genUserGroup(UserGroup userGroup, Long count);

    Long evaluateUserGroup(UserGroup userGroup);

    void   refreshUserGroup(String userGroupId, String busiDate);
}
