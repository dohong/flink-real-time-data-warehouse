package com.jolohong.TagUser.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.FileInfo;
import com.jolohong.TagUser.mapper.FileInfoMapper;
import com.jolohong.TagUser.service.FileInfoService;
import org.springframework.stereotype.Service;

@Service
@DS("mysql")
public class FileInfoServiceImpl extends ServiceImpl<FileInfoMapper, FileInfo> implements FileInfoService {
}
