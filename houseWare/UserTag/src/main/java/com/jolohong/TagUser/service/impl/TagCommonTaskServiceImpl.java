package com.jolohong.TagUser.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.FileInfo;
import com.jolohong.TagUser.bean.TagCommonTask;
import com.jolohong.TagUser.mapper.TagCommonTaskMapper;
import com.jolohong.TagUser.service.FileInfoService;
import com.jolohong.TagUser.service.TagCommonTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DS("mysql")
public class TagCommonTaskServiceImpl extends ServiceImpl<TagCommonTaskMapper, TagCommonTask> implements TagCommonTaskService {
    @Autowired
    FileInfoService fileInfoService;

    @Override
    public TagCommonTask getTagCommonTaskWithJarFile(Long id) {
        TagCommonTask tagCommonTask =getById(id);
        if(tagCommonTask!=null){
            FileInfo fileInfo =fileInfoService.getById(tagCommonTask.getTaskFileId());
            tagCommonTask.setFileInfo(fileInfo);
        }
        return tagCommonTask;
    }
}
