package com.jolohong.TagUser.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.TagInfo;
import com.jolohong.TagUser.mapper.TagInfoMapper;
import com.jolohong.TagUser.service.TagInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@DS("mysql")
public class TagInfoServiceImpl extends ServiceImpl<TagInfoMapper, TagInfo> implements TagInfoService {

    @Autowired
    TagInfoMapper tagInfoMapper;
    @Override
    public List<TagInfo> getTagInfoAllWithStatus() {
        return tagInfoMapper.getTagInfoAllWithStatus();
    }

    @Override
    public TagInfo getTagInfo(Long taskId) {
        TagInfo tagInfo=getById(taskId);
        if(tagInfo.getTagLevel()>1L){
            TagInfo parentTagInfo = getById(tagInfo.getParentTagId());
            tagInfo.setParentTagLevel(parentTagInfo.getTagLevel());
            tagInfo.setParentTagName(parentTagInfo.getTagName());
            tagInfo.setParentTagCode(parentTagInfo.getTagCode());
        }
        return tagInfo;
    }

    @Override
    public List<TagInfo> getTagValueList(String parentTagCode) {

        return tagInfoMapper.getTagValueList(parentTagCode);
    }

    @Override
    public Map<String, TagInfo> getTagInfoMapWithCode() {
        List<TagInfo> tagInfoList=list();
        Map<String, TagInfo> tagInfoMap=new HashMap<>();
        for (TagInfo tagInfo : tagInfoList) {
            tagInfoMap.put(tagInfo.getTagCode(), tagInfo);
        }
        return tagInfoMap;
    }

    @Override
    public boolean removeById(Serializable id) {
        System.out.println("删除标签  id"+id);
        boolean status = tagInfoMapper.deleteById(id) == 1;
        System.out.println("删除标签情况: " + status);
        return status;
    }
}
