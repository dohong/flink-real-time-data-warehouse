package com.jolohong.TagUser.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.FileInfo;
import com.jolohong.TagUser.bean.TagInfo;
import com.jolohong.TagUser.bean.TaskInfo;
import com.jolohong.TagUser.bean.TaskTagRule;
import com.jolohong.TagUser.constants.ConstCodes;
import com.jolohong.TagUser.mapper.TaskInfoMapper;
import com.jolohong.TagUser.service.FileInfoService;
import com.jolohong.TagUser.service.TagInfoService;
import com.jolohong.TagUser.service.TaskInfoService;
import com.jolohong.TagUser.service.TaskTagRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DS("mysql")
public class TaskInfoServiceImpl extends ServiceImpl<TaskInfoMapper, TaskInfo> implements TaskInfoService {

    @Autowired
    TaskTagRuleService taskTagRuleService;

    @Autowired
    TagInfoService tagInfoService;

    @Autowired
    FileInfoService fileInfoService;

    @Override
    public void saveTaskInfoWithTag(TaskInfo taskInfo) {
        if(taskInfo.getId()!=null){
            TaskTagRule taskTagRule = new TaskTagRule();
            taskTagRule.setTaskId(taskInfo.getTagId());
            taskTagRuleService.remove(new QueryWrapper<TaskTagRule>()
                    .eq("task_id", taskInfo.getId()));
        }
        saveOrUpdate(taskInfo);
        List<TaskTagRule> taskTagRuleList = taskInfo.getTaskTagRuleList();
        if(taskTagRuleList!=null && taskTagRuleList.size()>0){
            for (TaskTagRule taskTagRule : taskTagRuleList) {
                taskTagRule.setTaskId(taskInfo.getId());
            }
            taskTagRuleService.saveBatch(taskTagRuleList);
        }
        if(taskInfo.getTaskType().equals(ConstCodes.TASK_TYPE_TAG)){
            TagInfo tagInfoForUpdate = new TagInfo();
            tagInfoForUpdate.setTagTaskId(taskInfo.getId());
            tagInfoForUpdate.setId(taskInfo.getTagId());
            tagInfoService.updateById(tagInfoForUpdate);
        }
    }

    @Override
    public TaskInfo getTaskInfoWithTag(Long taskId) {
        TaskInfo taskInfo = getById(taskId);
        if(taskInfo.getFileId()!=null){
            FileInfo fileInfo = fileInfoService.getById(taskInfo.getFileId());
            taskInfo.setFileName(fileInfo.getFileName());
            taskInfo.setFilePath(fileInfo.getFilePath());
        }
        List<TaskTagRule> taskTagRuleList = taskTagRuleService.list(new QueryWrapper<TaskTagRule>()
                .eq("task_id", taskId));
        taskInfo.setTaskTagRuleList(taskTagRuleList);
        return taskInfo;
    }
}
