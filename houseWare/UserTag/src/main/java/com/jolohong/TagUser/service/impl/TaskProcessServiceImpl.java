package com.jolohong.TagUser.service.impl;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.TaskInfo;
import com.jolohong.TagUser.bean.TaskProcess;
import com.jolohong.TagUser.mapper.TaskProcessMapper;
import com.jolohong.TagUser.service.TaskInfoService;
import com.jolohong.TagUser.service.TaskProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.jolohong.TagUser.constants.ConstCodes.*;

@Service
@DS("mysql")
public class TaskProcessServiceImpl extends ServiceImpl<TaskProcessMapper, TaskProcess> implements TaskProcessService {

    @Autowired
    TaskInfoService taskInfoService;

    @Autowired
    TaskProcessMapper taskProcessMapper;

    @Override
    public void updateStatus(Long taskProcessId, String status) {
        updateStatus(taskProcessId,null, status);
    }

    @Override
    public void updateStatus(Long taskProcessId, String yarnAppId, String status) {
        TaskProcess taskProcess = new TaskProcess();
        taskProcess.setId(taskProcessId);
        taskProcess.setYarnAppId(yarnAppId);
        taskProcess.setTaskExecStatus(status);
        System.out.println("---》任务状态更新：  "+status);
        if(status.equals(TASK_EXEC_STATUS_START)){
            taskProcess.setStartTime(new Date());
        }else if (status.equals(TASK_EXEC_STATUS_FAILED)||status.equals(TASK_EXEC_STATUS_FINISHED)){
            taskProcess.setEndTime(new Date());
        }
        saveOrUpdate(taskProcess);
    }

    @Override
    public void genTaskProcess(String taskDate) {
        List<TaskInfo> taskInfoList = taskInfoService.list(new QueryWrapper<TaskInfo>().eq("task_status", TASK_STATUS_ON));
        String batchID = UUID.randomUUID().toString();
        System.out.println(taskInfoList.size()+taskInfoList.toString());
        List<TaskProcess> taskProcessList = taskInfoList.stream().map(taskInfo -> {
            return TaskProcess.builder()
                    .taskId(taskInfo.getId())
                    .taskName(taskInfo.getTaskName())
                    .taskExecTime(taskInfo.getTaskTime())
                    .taskExecLevel(taskInfo.getTaskExecLevel())
                    .taskBusiDate(taskDate)
                    .taskExecStatus(TASK_EXEC_STATUS_TODO)
                    .batchId(batchID)
                    .createTime(new Date()).build();
        }).collect(Collectors.toList());
        System.out.println("打印任务列表： "+taskProcessList);
        for (TaskProcess taskProcess : taskProcessList) {
            System.out.println("执行： "+taskProcess.getTaskId()+"提交情况： "+save(taskProcess));
        }
    }

    @Override
    public List<TaskProcess> getTodoTaskProcessList(String taskTime) {
        return taskProcessMapper.getTodoTaskProcessList(taskTime);
    }
}
