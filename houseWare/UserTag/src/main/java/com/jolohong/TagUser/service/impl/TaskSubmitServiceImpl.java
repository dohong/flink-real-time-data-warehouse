package com.jolohong.TagUser.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.jolohong.TagUser.bean.SubmitEvent;
import com.jolohong.TagUser.bean.TagCommonTask;
import com.jolohong.TagUser.bean.TaskInfo;
import com.jolohong.TagUser.bean.TaskProcess;
import com.jolohong.TagUser.service.TaskInfoService;
import com.jolohong.TagUser.service.TaskProcessService;
import com.jolohong.TagUser.service.TaskSubmitService;
import com.jolohong.TagUser.service.TagCommonTaskService;
import com.jolohong.TagUser.utils.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.jolohong.TagUser.constants.ConstCodes.*;

@Slf4j
@Service
@DS("mysql")
public class TaskSubmitServiceImpl  implements TaskSubmitService {

    @Autowired
    TaskInfoService taskInfoService;

    @Autowired
    TagCommonTaskService tagCommonTaskService;

    @Autowired
    TaskProcessService taskProcessService;

    @Value("${flink.rest.submitter.url}")
    String restSubmitterURL;

    @Override
    @Async
    public void submitTask(TaskProcess taskProcess, boolean isRetry) {
        TaskInfo taskInfo = taskInfoService.getTaskInfoWithTag(Long.valueOf(taskProcess.getTaskId()));
        taskProcessService.updateStatus(taskProcess.getId(), TASK_EXEC_STATUS_START);
        String params="taskid="+taskProcess.getId()+"&"+"mainclass="+taskInfo.getMainClass();
        try {
            HttpUtil.getT(restSubmitterURL, params);
            System.out.println("提交任务" + restSubmitterURL + "?" + params);
        }catch (Exception e){
            e.printStackTrace();
            taskProcessService.updateStatus(taskProcess.getTaskId(),TASK_EXEC_STATUS_FAILED);
        }

    }
    public void submitToRestSubniter(SubmitEvent submitEvent){
        try{
            String jsonString = JSON.toJSONString(submitEvent);
            HttpUtil.post(restSubmitterURL, jsonString);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("远程提交失败");
        }
    }
}
