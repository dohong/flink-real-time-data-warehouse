package com.jolohong.TagUser.service.impl;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.TaskTagRule;
import com.jolohong.TagUser.mapper.TaskTagRuleMapper;
import com.jolohong.TagUser.service.TaskTagRuleService;
import org.springframework.stereotype.Service;

@Service
@DS("mysql")
public class TaskTagRuleServiceImpl extends ServiceImpl<TaskTagRuleMapper, TaskTagRule> implements TaskTagRuleService {

}
