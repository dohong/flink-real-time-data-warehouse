package com.jolohong.TagUser.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TagUser.bean.TagCondition;
import com.jolohong.TagUser.bean.TagInfo;
import com.jolohong.TagUser.bean.UserGroup;
import com.jolohong.TagUser.mapper.UserGroupMapper;
import com.jolohong.TagUser.service.TagInfoService;
import com.jolohong.TagUser.service.UserGroupService;
import com.jolohong.TagUser.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jolohong.TagUser.constants.ConstCodes.*;

@Service
@Slf4j
@DS("mysql")
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper, UserGroup> implements UserGroupService {
    @Autowired
    TagInfoService tagInfoService;

    //1负责保存分群基本信息 mysql    mybatis-plus
    // 2  根据分群中的条件筛选出人群包(基于bitmap的组合查询)
    // 3 保存分群的人群包 -> clickhouse
    //  4  保存分群的人群包 -> redis

    @Override
    public void genUserGroup(UserGroup userGroup, Long count) {
        //1负责保存分群基本信息 mysql  （）
        //  condition_json_str 分群条件(json)
        String conditionJsonStr = JSON.toJSONString(userGroup.getTagConditions());
        userGroup.setConditionJsonStr(conditionJsonStr);
        //   condition_commentvarchar  分群条件(中文描述)
        String conditionComment = userGroup.conditionJsonToComment();
        userGroup.setConditionComment(conditionComment);
        // create_time
        userGroup.setCreateTime(new Date());
        System.out.println("用户群体：   "+userGroup);
        this.saveOrUpdate(userGroup);//会把新生成的主键id 写入到对象中 //mysql

        // 2.1  根据分群中的条件筛选出人群包(基于bitmap的组合查询)
        System.out.println(userGroup.getTagConditions() + "     上线时间" + userGroup.getBusiDate());
        String bitAndSQL = getBitAndSQL(userGroup.getTagConditions(), userGroup.getBusiDate());

        System.out.println(bitAndSQL);

        // 4获得个数 更新到mysql中
//        Long userCount = baseMapper.getUserCountByUserGruopId(userGroup.getId().toString());
//        userGroup.setUserGroupNum(userCount);
        userGroup.setUserGroupNum(0L);
        userGroup.setUpdateTime(new Date());
        baseMapper.updateById(userGroup);
    }


    //insert into  user_group
    //select ....
    private String getInsertUidsSQL(String userGroupId,  String bitAndSql){
        String insertUidsSql="insert into user_group select '"+userGroupId+"',"+bitAndSql;
        return insertUidsSql;
    }

    private String getBitAndSQL(List<TagCondition> tagConditions, String taskDate) {
        Map<String, TagInfo> tagInfoMapWithCode = tagInfoService.getTagInfoMapWithCode();
        String bitsql=null;
        for (TagCondition condition : tagConditions) {
            if(bitsql==null){
                bitsql="("+getConditionSQL(condition,taskDate,tagInfoMapWithCode)+")";
            }else {
                bitsql=" bitmapAnd ( "+bitsql+",("  +getConditionSQL(condition,taskDate,tagInfoMapWithCode)+"))";
            }
        }
        return bitsql;
    }

    //(select  groupBitmapMergeState(us)
// from user_tag_value_string
// where  tag_code ='tg_person_base_gender' and tag_value='女性’
// and dt=‘2021-05-16’)
    //  1    表名           要 通过tag_code 查询mysql 中tag_info 中的tag_value_type
//   2    tag_code  ->  参数
//   3   tag_value     ->   参数
//        加不加单引     ->要查询mysql 中tag_info 中的tag_value_type
//        一个还是多个  -> 根据tagvalues 数组长度
//  4   符号       ->  要根据operator  进行转义 =   >=    in  not in
//  5   业务日期  -> 参数
    private  String  getConditionSQL(TagCondition tagCondition, String taskDate , Map<String,TagInfo> tagInfoWithTagCodeMap){
        String tagCode = tagCondition.getTagCode();
        TagInfo tagInfo = tagInfoWithTagCodeMap.get(tagCode);
        String tableName= getTableName(tagInfo.getTagValueType() ); //根据tagValueType 得到表名
        List<String> tagValues = tagCondition.getTagValues();
        String tagValueSQL = getTagValueSQL(tagValues, tagInfo.getTagValueType(), tagCondition.getOperator());

        String conditionSQL="select  groupBitmapMergeState(us)  from  " +
                ""+tableName+ " where  tag_code ='"+tagCode.toLowerCase()+"' and "+tagValueSQL+" and  dt='"+taskDate+"'";

        return conditionSQL;
    }

    //根据 相关条件组合tag_value的判断表达式 如：tag_value in ('70后','80后’)
    //   3   tag_value     ->   参数
    //        加不加单引     ->要查询mysql 中tag_info 中的tag_value_type
    //        一个还是多个  -> 根据tagvalues 数组长度

    private String getTagValueSQL(  List<String> tagValues,  String  tagValueType ,String operatorStr  ){

        String values=null;
        if(tagValueType.equals(TAG_VALUE_TYPE_DATE)||tagValueType.equals(TAG_VALUE_TYPE_STRING)){
            values = "'" + StringUtils.join(tagValues, "','") + "'";
        }else{
            values =   StringUtils.join(tagValues, ",")  ;
        }
        if(tagValues.size()>1){
            values="("+values+")";
        }
        String conditionOperator = getConditionOperator(operatorStr);

        return  "tag_value "+conditionOperator+" "+ values;

    }

    //根据英文判断符号转为 sql判断符号
    private  String getConditionOperator(String operator){
        switch (operator){
            case "eq":
                return "=";
            case "lte":
                return "<=";
            case "gte":
                return ">=";
            case "lt":
                return "<";
            case "gt":
                return ">";
            case "neq":
                return "<>";
            case "in":
                return "in";
            case "nin":
                return "not in";
        }
        throw  new RuntimeException("操作符不正确");
    }

    //根据tagValueType 得到表名
    private String getTableName(String tagValueType){
        String tableNamePrefix="user_tag_value_";
        if (tagValueType.equals(TAG_VALUE_TYPE_LONG)) {
            return tableNamePrefix+"long";
        }else if (tagValueType.equals(TAG_VALUE_TYPE_DECIMAL)) {
            return tableNamePrefix+"decimal";
        }else if (tagValueType.equals(TAG_VALUE_TYPE_STRING)) {
            return tableNamePrefix+"string";
        }else if (tagValueType.equals(TAG_VALUE_TYPE_DATE)) {
            return tableNamePrefix+"date";
        }else{
            throw new RuntimeException("类型不匹配！！");
        }

    }

    private String getBitAndSql(List<TagCondition> tagConditionList,String taskDate ){

        Map<String, TagInfo> tagInfoMapWithCode = tagInfoService.getTagInfoMapWithCode();

        String bitsql =null;

        for (TagCondition tagCondition : tagConditionList) {
            if(bitsql==null){
                bitsql = "("+getConditionSQL(tagCondition,taskDate,tagInfoMapWithCode)+")";
            }else{
                bitsql= " bitmapAnd ( "+bitsql+",("  +getConditionSQL(tagCondition,taskDate,tagInfoMapWithCode)+"))";
            }

        }
        return    bitsql;

    }

    @Override
    public Long evaluateUserGroup(UserGroup userGroup) {
        String bitAndSql = getBitAndSql(userGroup.getTagConditions(), userGroup.getBusiDate());
        String  countUserSql=" select   bitmapCardinality("+bitAndSql +")";
        Long userCount = baseMapper.getUserCountBySQL(countUserSql);
        return userCount;
    }

    @Override
    public void refreshUserGroup(String userGroupId, String busiDate) {
        System.out.println("查找用户群id "+userGroupId);
//        //清除原来的人群包信息
//        baseMapper.deleteUserCountById(userGroupId);
//        //重新查询人群包  依据条件
//        UserGroup userGroup = baseMapper.selectById(userGroupId);
//        String conditionJsonStr = userGroup.getConditionJsonStr();
//        List<TagCondition> tagConditions = JSON.parseArray(conditionJsonStr, TagCondition.class);
//        userGroup.setTagConditions(tagConditions);
//        userGroup.setBusiDate(busiDate);


        UserGroup userGroup = baseMapper.getUserGroupbyid(userGroupId);
        String conditionJsonStr = userGroup.getConditionJsonStr();
        String conditionSQL = getConditionSQL(conditionJsonStr);
        String SQL="select count(*) from user_tag_merge_"+busiDate.replace("-","");
        System.out.println("用户数执行SQL "+SQL);
        Long userCount = baseMapper.getUserCountBySQL(SQL + conditionSQL);
        System.out.println("预估用户数：  "+userCount);
        userGroup.setUserGroupNum(userCount);
        userGroup.setUpdateTime(new Date());
        saveOrUpdate(userGroup);
//        genUserGroup(userGroup);

    }

    public String getConditionSQL(String conditionJsonStr){
        StringBuffer condition = new StringBuffer();
        JSONArray objects = JSON.parseArray(conditionJsonStr);
        for (int i = 0; i < objects.size(); i++) {
            String tagtask = objects.get(i).toString();
            JSONObject jsonObject = JSON.parseObject(tagtask);
            condition.append(jsonObject.getString("tagCode")+" in " + jsonObject.getString("tagValues").replace("\"","\'"));
            if(objects.size()-1!=i){
                condition.append(" and ");
            }
        }
        return " where " +condition.toString();
    }
}
