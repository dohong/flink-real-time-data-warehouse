import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;

public class jsontsets {
    public static void main(String[] args) {
        JSONArray objects = JSON.parseArray("[{\"operator\":\"in\",\"operatorName\":\"包含\",\"tagCode\":\"user_gender\",\"tagCodePath\":[\"dwd_order_wide\",\"user_attribute\",\"user_gender\"],\"tagName\":\"用户性别\",\"tagValues\":[\"男性\"]},{\"operator\":\"in\",\"operatorName\":\"包含\",\"tagCode\":\"user_ages\",\"tagCodePath\":[\"dwd_order_wide\",\"user_attribute\",\"user_ages\"],\"tagName\":\"年龄组\",\"tagValues\":[\"80后\",\"90后\",\"70后\"]}]");
        for (int i = 0; i < objects.size(); i++) {
            String tag = objects.get(i).toString();
            JSONObject jsonObject = JSON.parseObject(tag);
            System.out.println(jsonObject.getString("tagCode") +" in "+
                    jsonObject.getString("tagValues").replace("\"","\'"));
            if(objects.size()-1!=i){
                System.out.println(" and ");
            }
        }
    }
}
