package com.jolohong.TaskTag.Bitmap;

import com.jolohong.TaskTag.Common.Constants.ConstCode;
import com.jolohong.TaskTag.Common.Bean.TagInfo;
import com.jolohong.TaskTag.Common.Dao.TagInfoDao;
import com.jolohong.TaskTag.Common.Utils.ClickHouseUtils;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskBitmapApp {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME", "jolohong");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().useBlinkPlanner().build();
        TableEnvironment tableEnvironment = TableEnvironment.create( settings);
        String taskDate="2022-05-11";
        List<TagInfo> tagInfoListOnTask = TagInfoDao.getTagInfoListOnTask();
        ArrayList<TagInfo> tagInfoStringList = new ArrayList<>();
        ArrayList<TagInfo> tagInfoLongList = new ArrayList<>();
        ArrayList<TagInfo> tagInfoDecimalList = new ArrayList<>();
        ArrayList<TagInfo> tagInfoDeteList = new ArrayList<>();
        StringBuffer cloumnsList = new StringBuffer();
        System.out.println(tagInfoListOnTask.size());
        for (TagInfo tagInfo : tagInfoListOnTask) {
            if(tagInfo.getTag_value_type()==ConstCode.TAG_VALUE_TYPE_LONG){
                tagInfoLongList.add(tagInfo);
            }
            if(tagInfo.getTag_value_type()==ConstCode.TAG_VALUE_TYPE_DECIMAL){
                tagInfoDecimalList.add(tagInfo);
            }
            if(tagInfo.getTag_value_type()==ConstCode.TAG_VALUE_TYPE_STRING){
                tagInfoStringList.add(tagInfo);
            }
            if(tagInfo.getTag_value_type()==(ConstCode.TAG_VALUE_TYPE_DATE)){
                tagInfoDeteList.add(tagInfo);
            }
        }
        insertSQLByTagType("user_tag_value_string", tagInfoStringList, taskDate);
        insertSQLByTagType("user_tag_value_long", tagInfoLongList, taskDate);
        insertSQLByTagType("user_tag_value_decimal", tagInfoDecimalList, taskDate);
        insertSQLByTagType("user_tag_value_date", tagInfoDeteList, taskDate);


    }

    private static void insertSQLByTagType(String tableName, List<TagInfo> tagList, String taskDate) throws SQLException, ClassNotFoundException {
        String dropTableSQL="alter table "+tableName+" delete where dt='" + taskDate+"'";
        System.out.println("执行删除表： "+dropTableSQL);
//        ClickHouseUtils.executeSQL(dropTableSQL);
        StringBuffer tagCodeSql = new StringBuffer();
        if(tagList.size()>0){
            for (int i = 0; i < tagList.size(); i++) {
                String tag_code = tagList.get(i).getTag_code();
                tagCodeSql.append("('" + tag_code + "','" + tag_code + "')");
                if(i!=tagList.size()-1){
                    tagCodeSql.append(",");
                }
            }
            String insertSQL="insert into "+tableName +"select \n"+
                    "tag.1 AS tag_code,\n"+
                    "tag.2 AS tag_value,\n"+
                    "groupBitmapState(cast( uid as UInt64)) AS us\n"+
                    "'"+taskDate+"'\n"+
                    "from\n"+
                    "( select uid, arrayJoin(["+tagCodeSql+"]) AS tag\n"+
                    "from user_tag_merge_"+taskDate.replace("-","")+") AS ut\n"+
                    "where tag.2 <> ''\n"+
                    "group by tag.1, tag.2";
            System.out.println(insertSQL);
//            ClickHouseUtils.executeSQL(insertSQL);

        }

    }
}
