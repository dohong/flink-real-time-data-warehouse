package com.jolohong.TaskTag.Common.Bean;

import lombok.Data;

import java.util.Date;

@Data
public class TagInfo {
    String id;
    String tag_code;
    String tag_name;
    Long tag_level;
    Long parent_tag_id;
    String tag_type;
    Long tag_value_type;
    Long tag_value_limit;
    Long tag_value_step;
    Long tag_task_id;
    String tag_comment;
    Date create_time;
    Date update_time;
}
