package com.jolohong.TaskTag.Common.Bean;

import lombok.Data;

import java.util.Date;

@Data
public class TaskInfo {
    Long id=0L;
    String task_name;
    String task_status;
    String task_comment;
    String task_time;
    String task_type;
    String exec_type;
    String main_class;
    Long file_id=-1L;
    String task_args;
    String task_sql;
    String task_exec_level;
    Date create_time;
}
