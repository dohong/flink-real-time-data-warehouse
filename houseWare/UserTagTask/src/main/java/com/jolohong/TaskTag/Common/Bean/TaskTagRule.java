package com.jolohong.TaskTag.Common.Bean;

import lombok.Data;

@Data
public class TaskTagRule {
    Long id;
    Long tag_id;
    Long task_id;
    String query_value;
    Long sub_tag_id;
    String sub_tag_value;
}
