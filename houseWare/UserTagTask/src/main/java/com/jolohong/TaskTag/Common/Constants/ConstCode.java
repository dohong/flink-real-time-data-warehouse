package com.jolohong.TaskTag.Common.Constants;

public class ConstCode {
    public static final int  TAG_VALUE_TYPE_LONG=1;
    public static final int  TAG_VALUE_TYPE_DECIMAL=2;
    public static final int TAG_VALUE_TYPE_STRING=3;
    public static final int TAG_VALUE_TYPE_DATE=4;

    public static final String TASK_PROCESS_SUCCESS="1";
    public static final String TASK_PROCESS_ERROR="2";

    public static final String TASK_STAGE_START="1";
    public static final String TASK_STAGE_RUNNING="2";
}
