package com.jolohong.TaskTag.Common.Dao;

import com.jolohong.TaskTag.Common.Bean.TagInfo;
import com.jolohong.TaskTag.Common.Utils.MySQLUtils;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class TagInfoDao {
    public static TagInfo getTagInfoByTaskId(String taskId) throws Exception{
        TagInfo value =(TagInfo) MySQLUtils.queryOne("select * from tag_info where tag_task_id='"+taskId+"'", TagInfo.class, true);
        if (value==null){
            throw new RuntimeException("no this taskId ='"+taskId+"' for tag !");
        }
        return value;
    }
    public static List<TagInfo> getTagInfoListOnTask() throws SQLException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        List<TagInfo> queryList = MySQLUtils.queryList("select tg.* from tag_info tg join task_info tk on tg.tag_task_id = tk.id where  tk.task_status='1'   ", TagInfo.class, true);
        return queryList;
    }
}
