package com.jolohong.TaskTag.Common.Dao;

import com.jolohong.TaskTag.Common.Bean.TaskInfo;
import com.jolohong.TaskTag.Common.Utils.MySQLUtils;

import java.util.List;

public class TaskInfoDao {
    public static TaskInfo getTaskInfo(String taskId) throws Exception {
        String taskInfoSQL= "select id,task_name,"+
                "task_status,task_comment,task_time," +
                " task_type,exec_type,main_class,file_id,"+
                " task_args,task_sql,task_exec_level,create_time"+
                " from task_info where id='"+taskId+"'";
        TaskInfo queryOne = (TaskInfo) MySQLUtils.queryOne(taskInfoSQL, TaskInfo.class, true);
        if(queryOne==null){
            throw new RuntimeException("no task for task_id: "+taskId);
        }
        return queryOne;
    }
}
