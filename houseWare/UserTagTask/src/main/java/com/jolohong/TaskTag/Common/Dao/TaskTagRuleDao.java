package com.jolohong.TaskTag.Common.Dao;

import com.jolohong.TaskTag.Common.Bean.TaskTagRule;
import com.jolohong.TaskTag.Common.Utils.MySQLUtils;

import java.util.List;

public class TaskTagRuleDao {
    public static List<TaskTagRule> getTaskTagRuleListByTaskId(String taskId) throws Exception {
        String taskRuleSQL="select tr.id,tr.tag_id,tr.task_id,tr.query_value, \n"+
        "sub_tag_id,ti.tag_name as sub_tag_value \n"+
        "from task_tag_rule tr,tag_info ti \n"+
        "where tr.sub_tag_id=ti.id and  tr.task_id='"+taskId+"'";
        List<TaskTagRule> taskTagRules = MySQLUtils.queryList(taskRuleSQL, TaskTagRule.class, true);
        return taskTagRules;
    }
}
