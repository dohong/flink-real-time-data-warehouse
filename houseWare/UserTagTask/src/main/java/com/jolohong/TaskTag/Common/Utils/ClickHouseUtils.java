package com.jolohong.TaskTag.Common.Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ClickHouseUtils {
    private static Properties properties=null;
    static {
        properties=PropertiesUtil.load("conf.properties");
    }
    private static final String CLICKHOUSE_URL=properties.getProperty("clickhouse.url");
    private static final String CLICKHOUSE_DRIVER="ru.yandex.clickhouse.ClickHouseDriver";
    public static void executeSQL(String SQL) throws ClassNotFoundException, SQLException {
        Class.forName(CLICKHOUSE_DRIVER);
        System.out.println("clickhouse url: "+CLICKHOUSE_URL);
        Connection connection = DriverManager.getConnection(CLICKHOUSE_URL, null, null);
        Statement statement = connection.createStatement();
        System.out.println("执行clickhouse语句："+SQL);
        statement.execute(SQL);
        connection.close();
    }
    public static Connection getConnection() throws Exception {
        Class.forName(CLICKHOUSE_DRIVER);
        return DriverManager.getConnection(CLICKHOUSE_URL,null, null);
    }
}
