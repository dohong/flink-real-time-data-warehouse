package com.jolohong.TaskTag.Common.Utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.hadoop.yarn.util.ConverterUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MySQLUtils {
    private static final Properties properties=PropertiesUtil.load("conf.properties");
    private static final String DB_DRIVER=properties.getProperty("db.driver");
    private static final String DB_URL=properties.getProperty("db.url");
    private static final String DB_USERNAME=properties.getProperty("db.username");
    private static final String DB_PASSWORD=properties.getProperty("db.password");

    public static List queryList(String SQL) throws ClassNotFoundException, SQLException {
        Class.forName(DB_DRIVER);
        List<JSONObject> arrayList = new ArrayList<>();
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        System.out.println(SQL);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL);
        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next()){
            JSONObject jsonObject = new JSONObject();
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                jsonObject.put(metaData.getColumnName(i),resultSet.getObject(i));
            }
            arrayList.add(jsonObject);
        }
        statement.close();
        connection.close();
        return arrayList;
    }

    public static <T> List<T> queryList(String SQL, Class<T> clazz , Boolean underScoreToCamel) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Class.forName(DB_DRIVER);
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        System.out.println(SQL);
        List<T> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL);
        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next()){
            Object instance = clazz.newInstance();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String propertyName = metaData.getColumnLabel(i);
                if(underScoreToCamel){
                    propertyName = metaData.getColumnLabel(i).toLowerCase();
                }
                ConvertUtils.register(new DateConverter(null), java.util.Date.class);
                BeanUtils.setProperty(instance, propertyName, resultSet.getObject(i));
            }
            list.add((T)instance);
        }
        System.out.println("子标签数: "+ list.size());
        statement.close();
        connection.close();
        return list;
    }
    public static <T> Object queryOne(String SQL, Class<T> clazz, Boolean underScoreToCamel) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        System.out.println(SQL);
        Class.forName(DB_DRIVER);
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQL);
        ResultSetMetaData metaData = resultSet.getMetaData();
        List<T> list = new ArrayList<>();
        while (resultSet.next()){
            T value = clazz.newInstance();
            for (int i = 1; i < metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                if(underScoreToCamel){
                    columnName = columnName.toLowerCase();
                }
                ConvertUtils.register(new DateConverter(null), java.util.Date.class);
                BeanUtils.setProperty(value,columnName, resultSet.getObject(i));
            }

            list.add(value);
        }
        statement.close();
        connection.close();
        if(list!=null&& list.size()!=0) return list.get(0);
        else return null;
    }


    public static <T> void insertOne(String SQL, T obj) throws ClassNotFoundException, SQLException, IllegalAccessException {
        Class.forName(DB_DRIVER);
        List<T> list=new ArrayList<T>();
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            Object value = field.get(obj);
            preparedStatement.setObject(i+1, value);
        }
        boolean execute = preparedStatement.execute();
        System.out.println("插入数据的执行情况："+execute);
    }
}
