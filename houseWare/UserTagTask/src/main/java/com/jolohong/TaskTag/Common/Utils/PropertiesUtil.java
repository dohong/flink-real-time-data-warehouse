package com.jolohong.TaskTag.Common.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    public static Properties load(String PropertiesPath) {
        Properties properties = new Properties();
        InputStream resourceAsStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(PropertiesPath);
        try {
            properties.load(resourceAsStream);
        }catch (IOException e){
            System.exit(1);
        }
        return properties;
    }
}
