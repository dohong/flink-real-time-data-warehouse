package com.jolohong.TaskTag.Common.Utils;

import com.jolohong.TaskTag.Common.Dao.TransientSink;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.types.Row;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class SinkClickHouseUtils extends RichSinkFunction<Row> {
    private static final Properties properties=PropertiesUtil.load("conf.properties");
    private static final String CLICKHOUSE_URL=properties.getProperty("clickhouse.url");
    private static final String CLICKHOUSE_DRIVER="ru.yandex.clickhouse.ClickHouseDriver";
    Connection connection=null;
    String SQL;
    public SinkClickHouseUtils(String SQL){
        this.SQL=SQL;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        connection=ClickHouseUtils.getConnection();
    }

    @Override
    public void invoke(Row value, Context context) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        String[] split = value.toString()
                .replace("+I[", "")
                .replace("]", "")
                .replace(" ","")
                .split(",");
        for (int i = 0; i < split.length; i++) {
            if(i==0){
                preparedStatement.setLong(i+1,Long.parseLong(split[i]));
            }else
            preparedStatement.setString(i+1,split[i]);
        }
        preparedStatement.execute();
        connection.commit();
        System.out.println("数据插入成功  ");
    }

    @Override
    public void close() throws Exception {
        super.close();
        if(connection!=null){
            connection.close();
        }
    }
}
