package com.jolohong.TaskTag.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TaskTag.TaskServer.dao.FileInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface FileInfoMapper extends BaseMapper<FileInfo> {
    @Select("select * from file_info\n" +
            "where id=#{id}")
    public FileInfo getfileinfovalue(Long id);

}
