package com.jolohong.TaskTag.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcessLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TaskProcessLogMapper extends BaseMapper<TaskProcessLog> {
}
