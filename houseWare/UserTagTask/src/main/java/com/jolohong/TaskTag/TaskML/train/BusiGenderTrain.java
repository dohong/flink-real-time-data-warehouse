package com.jolohong.TaskTag.TaskML.train;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;

public class BusiGenderTrain {
    public static void main(String[] args) {
        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setParallelism(1);
        String taskDate=args[1];
        String genderQuerySQL="with uid_visit as (\n" +
                "select   user_id  as uid, category1_id ,during_time  from dwd_page_log pl" +
                "join dim_sku_info si on  si.id=page_item" +
                " where  page_id='good_detail' and page_item_type='sku_id'" +
                "and pl.dt='"+taskDate+"' and si.dt='"+taskDate+"'" +
                ")," +
                " uid_label as (" +
                "select  id ,gender  from dim_user_info  where dt='9999-99-99' and  gender  is not null" +
                ")" +
                "select" +
                "uid_feature.uid," +
                "male_dur," +
                "female_dur," +
                "c1_1," +
                "c1_2," +
                "c1_3," +
                "uid_label.gender" +
                "from" +
                "(" +
                " select" +
                "sum(if(category1_id in (3,4,6,16) ,dur_time,0)) male_dur," +
                "sum(if(category1_id in (8,12,15) ,dur_time,0)) female_dur," +
                "sum(if(rk=1,category1_id,0)) c1_1," +
                "sum(if(rk=2,category1_id,0)) c1_2," +
                "sum(if(rk=3,category1_id,0)) c1_3" +
                "from" +
                "(" +
                "select uid,category1_id,sum(during_time) dur_time,  count(*) ct ," +
                "row_number()over(partition by uid order by count(*) desc ) rk" +
                "from uid_visit  uv" +
                "group by uid,category1_id" +
                ") uv_rk" +
                "where  rk<=3" +
                "group by uid" +
                ") uid_feature  join uid_label on uid_feature.uid=uid_label.id";
        System.out.println(genderQuerySQL);
        DecisionTreeClassifierPipeline gender = new DecisionTreeClassifierPipeline().setLabelColname("gender")
                .setFeatureColName(new String[]{"male_dur", "female_dur", "c1_1", "c1_2", "c1_3"})
                .setMaxCategories(20)
                .setMaxDepth(7)
                .setMaxBins(32)
                .setMinInfoGain(0.03)
                .setMinInstancesPerNode(3)
                .init();
        gender.train(null);




    }
}
