package com.jolohong.TaskTag.TaskML.train;

import com.alibaba.alink.operator.batch.BatchOperator;
import com.alibaba.alink.operator.batch.evaluation.EvalMultiClassBatchOp;
import com.alibaba.alink.operator.batch.regression.LinearRegModelInfoBatchOp;
import com.alibaba.alink.operator.batch.source.MemSourceBatchOp;
import com.alibaba.alink.operator.common.evaluation.MultiClassMetrics;
import com.alibaba.alink.pipeline.Pipeline;
import com.alibaba.alink.pipeline.PipelineModel;
import com.alibaba.alink.pipeline.TransformerBase;
import com.alibaba.alink.pipeline.classification.DecisionTreeClassifier;
import com.alibaba.alink.pipeline.regression.LinearRegressionModel;
import com.alibaba.alink.pipeline.sql.Select;
import org.apache.flink.types.Row;

import java.util.Collections;
import java.util.List;

public class DecisionTreeClassifierPipeline {
    private Pipeline pipeline=null;
    private PipelineModel pipelineModel=null;

    public DecisionTreeClassifierPipeline init(){
        pipeline = new Pipeline().add(
                new Select().setClause(labelColname)
        ).add(
                new DecisionTreeClassifier()
                        .setLabelCol("label_index")
                        .setFeatureCols("feature_index")
                        .setPredictionCol("prediction_col")
                        .setMinInfoGain(minInfoGain)
                        .setMinSamplesPerLeaf(minInstancesPerNode)
                        .setMaxBins(maxBins)
                        .setMaxDepth(maxDepth)
                        .setPredictionCol("pred")
        );
        return null;
    }

    public DecisionTreeClassifierPipeline(){
        new DecisionTreeClassifier()
                .setLabelCol("label_index")
                .setFeatureCols("feature_index")
                .setPredictionCol("prediction_col")
                .setMinInfoGain(minInfoGain)
                .setMinSamplesPerLeaf(minInstancesPerNode)
                .setMaxBins(maxBins)
                .setMaxDepth(maxDepth);
    }
    public void train(BatchOperator data){
        pipelineModel = pipeline.fit(data);
    }
    public BatchOperator predict(BatchOperator data){
        BatchOperator<?> transform = pipelineModel.transform(data);
        return transform;
    }

    public String getDecisionTree(){
        TransformerBase<?>[] transformers = pipelineModel.getTransformers();
        return transformers.toString();
    }
    public void getFeatureWeight() throws Exception {
        ((LinearRegressionModel) pipelineModel.getTransformers()[1]).getModelData()
                .link(new LinearRegModelInfoBatchOp()
                        .lazyPrintModelInfo()
                );
        BatchOperator.execute();
    }
    public void printEvaluate(List<Row> df){
        MemSourceBatchOp inop = new MemSourceBatchOp(df, "label string, detailInput string");
        MultiClassMetrics metrics = new EvalMultiClassBatchOp()
                .setLabelCol("lable")
                        .setPredictionCol("detailInput")
                                .linkFrom(inop)
                                        .collectMetrics();
        System.out.println(metrics.getAccuracy());
        System.out.println(metrics.getRecall(""));
        System.out.println("Macro Precision: "+metrics.getMacroPrecision());
        System.out.println("Micro Recall: "+metrics.getMicroRecall());
        System.out.println("Weighted Sensitivity: "+metrics.getWeightedSensitivity());
    }

    public void saveModel(String path){
        pipelineModel.save(path,true);
    }
    public void loadModel(String path){
        pipelineModel = PipelineModel.load(path);
    }

    public void convertLabelToOrigin() throws Exception {
        TransformerBase<?>[] transformers = pipelineModel.getTransformers();
        for (int i = 0; i < transformers.length; i++) {
            System.out.println(i + "\t" + transformers[i]);
        }
        ((LinearRegressionModel) transformers[1]).getModelData()
                .link(
                        new LinearRegModelInfoBatchOp()
                                .lazyPrintModelInfo()
                );
        BatchOperator.execute();
    }



    private String labelColname=null;
    private String[] featureColNames=null;
    //最大分类树（用于识别连续值特征和分类特征）
    private Integer maxCategories=5;
    // 最大分支数
    private Integer maxBins=32;
    // 最大树深度
    private Integer maxDepth=5;
    //最小分支包含数据条数
    private Integer minInstancesPerNode=1;
    //最小分支信息增益
    private Double minInfoGain=0.0;

    DecisionTreeClassifierPipeline setMaxCategories(int maxCategories){
        this.maxCategories=maxCategories;
        return this;
    }
    DecisionTreeClassifierPipeline setMaxBins(int maxBins){
        this.maxBins=maxBins;
        return this;
    }
    DecisionTreeClassifierPipeline setMaxDepth(int maxDepth){
        this.maxDepth=maxDepth;
        return this;
    }

    DecisionTreeClassifierPipeline setMinInstancesPerNode(int minInstancesPerNode){
        this.minInstancesPerNode=minInstancesPerNode;
        return this;
    }

    DecisionTreeClassifierPipeline setMinInfoGain(Double minInfoGain){
        this.minInfoGain=minInfoGain;
        return this;
    }



    DecisionTreeClassifierPipeline setLabelColname(String labelColname){
        this.labelColname=labelColname;
        return this;
    }

    DecisionTreeClassifierPipeline setFeatureColName(String[] featureColNames){
        this.featureColNames=featureColNames;
        return this;
    }



}
