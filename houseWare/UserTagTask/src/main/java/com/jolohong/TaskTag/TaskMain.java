package com.jolohong.TaskTag;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.jolohong.TaskTag.Mapper")
@ComponentScan(basePackages = {"com.jolohong.TaskTag.TaskServer.Collector","com.jolohong.TaskTag.TaskServer.Server","com.jolohong.TaskTag.TaskServer.dao"})
public class TaskMain {
    public static void main(String[] args) {
        SpringApplication.run(TaskMain.class, args);

    }
}
