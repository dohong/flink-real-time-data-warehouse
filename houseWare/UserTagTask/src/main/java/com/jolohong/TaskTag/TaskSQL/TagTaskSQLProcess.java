package com.jolohong.TaskTag.TaskSQL;

import com.jolohong.TaskTag.Common.Bean.TagInfo;
import com.jolohong.TaskTag.Common.Bean.TaskInfo;
import com.jolohong.TaskTag.Common.Bean.TaskTagRule;
import com.jolohong.TaskTag.Common.Constants.ConstCode;
import com.jolohong.TaskTag.Common.Dao.TagInfoDao;
import com.jolohong.TaskTag.Common.Dao.TaskInfoDao;
import com.jolohong.TaskTag.Common.Dao.TaskTagRuleDao;
import com.jolohong.TaskTag.Common.Utils.PropertiesUtil;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.catalog.hive.HiveCatalog;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

public class TagTaskSQLProcess{
    private String taskId;
    private String taskDate;
    public TagTaskSQLProcess(String taskId, String date){
        this.taskId=taskId;
        this.taskDate=date;

    }
    public Long start() throws Exception {
        long startTime = System.currentTimeMillis();
        System.setProperty("HADOOP_USER_NAME", "jolohong");
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().useBlinkPlanner().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);
        String catalogName="gmallcatalog";
        String defualtDataBase="gmalldb";
        String HiveConfDir="C:\\Users\\jolohong\\IdeaProjects\\houseWare\\UserTagTask\\src\\main\\resources";
        HiveCatalog hiveCatalog = new HiveCatalog(catalogName,defualtDataBase,HiveConfDir);
        tableEnvironment.registerCatalog(catalogName, hiveCatalog);
        tableEnvironment.useCatalog(catalogName);
        tableEnvironment.useDatabase(defualtDataBase);
        TaskInfo taskInfo = TaskInfoDao.getTaskInfo(taskId);
        TagInfo tagInfo = TagInfoDao.getTagInfoByTaskId(taskId);
        List<TaskTagRule> taskTagRuleList = TaskTagRuleDao.getTaskTagRuleListByTaskId(taskId);


        String tableName = tagInfo.getTag_code();
        String tagValueType=null;
        if (tagInfo.getTag_value_type() == ConstCode.TAG_VALUE_TYPE_LONG) {
            tagValueType="LONG";
        } else if (tagInfo.getTag_value_type() == ConstCode.TAG_VALUE_TYPE_STRING) {
            tagValueType="STRING";
        } else if (tagInfo.getTag_value_type() == ConstCode.TAG_VALUE_TYPE_DATE) {
            tagValueType="STRING";
        } else if (tagInfo.getTag_value_type() == ConstCode.TAG_VALUE_TYPE_DECIMAL) {
            tagValueType="DECIMAL(16,2)";
        }
        Properties properties = PropertiesUtil.load("conf.properties");
        String hdfs_url = properties.getProperty("hdfs-store.path");
        String createSQL="create table if not exists "+tableName+"\n" +
                "( uid string, tag_value "+tagValueType+") " +
                "comment '"+tagInfo.getTag_name()+"'\n" +
                "PARTITIONED BY (`dt` STRING  comment '分区字段') \n" +
                "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t'\n" +
                "LOCATION '"+ hdfs_url+tableName+"'\n";
        System.out.println("便签数据类型："+tagValueType+" 便签表：\n"+createSQL);

        String taskSQL = taskInfo.getTask_sql().replace("$dt", taskDate);
        String caseWhenSQL="";
        if(taskTagRuleList.size()>0){
            StringBuffer tagList = new StringBuffer();
            for (TaskTagRule taskTagRule : taskTagRuleList) {
                tagList.append("when " + taskTagRule.getQuery_value()
                        + " then '" + taskTagRule.getSub_tag_value() + "' \n");
                tagList.append(" ");
            }
            caseWhenSQL= "case " + tagList +" end  as  tag_value \n";
        }else {
            caseWhenSQL=" as  tag_value\n";
        }
        String selectSQL="select id, "+caseWhenSQL+" "+"from ("+taskSQL+" ) tv";
        String insertSelectSQL="insert into "+tableName+" partition(dt="+"'"+taskDate+"')  "+selectSQL;
        System.out.println(insertSelectSQL);
        System.out.println();
        System.out.println(selectSQL);
        tableEnvironment.sqlQuery(selectSQL).execute().print();
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.HIVE);
        tableEnvironment.executeSql("drop table "+tableName);
        tableEnvironment.executeSql(createSQL);
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.DEFAULT);
        tableEnvironment.executeSql(insertSelectSQL);
        return System.currentTimeMillis()-startTime;
    }

}
