package com.jolohong.TaskTag.TaskServer.Collector;

import com.jolohong.TaskTag.TaskSQL.TagTaskSQLProcess;
import com.jolohong.TaskTag.TaskServer.Server.FileService;
import com.jolohong.TaskTag.TaskServer.Server.TaskProcessLogService;
import com.jolohong.TaskTag.TaskServer.Server.TaskProcessService;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcess;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcessLog;
import com.jolohong.TaskTag.TaskTagMerge.TagMergeProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
//@RequestMapping("/api")
public class submitCollector {
    @Autowired
    TaskProcessLogService taskProcessLogService;
    @Autowired
    TaskProcessService taskProcessService;
    @Autowired
    FileService fileService;

    @Autowired(required = false)
    TaskProcess taskProcess;

    static {
        System.out.println("---------------submit collector----------------");
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String hello(){
        return "hong, ge";
    }

    @GetMapping(value = "/submit")
    public void submit(@RequestParam(value = "taskid", defaultValue = "0") String taskid,
                       @RequestParam(value = "date", defaultValue = "2022-05-11") String date,
                       @RequestParam(value = "mainclass", defaultValue = "com.jolohong.TaskTag.TaskSQL.TagTaskSQLProcess")String mainClass ){
        if(Long.parseLong(taskid)<0){
            return;
        }
        TaskProcessLog taskProcessLog=new TaskProcessLog();
        try {
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println("taskid  _>"+taskid);
            TaskProcess processbyId = taskProcessService.getTaskProcessbyId(Long.parseLong(taskid));
            if(processbyId==null ){
                System.out.println("任务运行中：  "+taskid+"不可以再次执行等待任务完成");
                return;
            }
            String yarnid = UUID.randomUUID().toString();
            processbyId.setYarnAppId(yarnid);


            long taskStartTime = System.currentTimeMillis();
            taskProcessLog.copyTaskProcess(processbyId);
            taskProcessLog.setCreateTime(simpleDateFormat.format(new Date()));
//            if(mainClass==null){
//                new TagTaskSQLProcess(taskid.toString(), date).start();
//            }
//            else if(mainClass.equals("user_merger")){
//                new TagMergeProcess(taskid.toString(), date).start();
//            }

            TagTaskSQLProcess tagTaskSQLProcess = new TagTaskSQLProcess(processbyId.getTaskId().toString(), date);
            Long rundate = tagTaskSQLProcess.start();
            long taskEndTime = System.currentTimeMillis();
            Long taskDate = taskEndTime - taskStartTime;
            taskProcessLog.setTaskDate(taskDate.toString());
            System.out.println("运行时长： "+rundate+"任务日志  " + taskProcessLog);
            taskProcessLog.setTaskStage("1");
            taskProcessLog.setTaskExecStatus("FINSHED");
            processbyId.setEndTime(new Date());
            taskProcessLogService.addTaskProcessLog(taskProcessLog);
            taskProcessService.saveOrUpdate(processbyId);
        }catch (Exception e){
            e.printStackTrace();
            taskProcessLog.setTaskStage("2");
            taskProcessLog.setTaskExecStatus("2");
            taskProcessLog.setTaskExecMsg(e.getMessage());
            taskProcessLogService.addTaskProcessLog(taskProcessLog);
        }
    }


}
