package com.jolohong.TaskTag.TaskServer.Server;

import com.jolohong.TaskTag.TaskServer.dao.FileInfo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface FileService extends IService<FileInfo>{
    public FileInfo getFileJar(Long id);
}
