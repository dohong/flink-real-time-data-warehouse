package com.jolohong.TaskTag.TaskServer.Server;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcess;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcessLog;

public interface TaskProcessLogService extends IService<TaskProcessLog> {
    public void addTaskProcessLog(TaskProcessLog taskProcessLog);

}
