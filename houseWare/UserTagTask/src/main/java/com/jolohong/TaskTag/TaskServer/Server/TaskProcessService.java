package com.jolohong.TaskTag.TaskServer.Server;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcess;

public interface TaskProcessService extends IService<TaskProcess> {
    public TaskProcess getTaskProcessbyId(Long id);
}
