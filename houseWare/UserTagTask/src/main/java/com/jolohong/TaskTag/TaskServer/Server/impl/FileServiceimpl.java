package com.jolohong.TaskTag.TaskServer.Server.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TaskTag.Mapper.FileInfoMapper;
import com.jolohong.TaskTag.TaskServer.Server.FileService;
import com.jolohong.TaskTag.TaskServer.dao.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServiceimpl extends ServiceImpl<FileInfoMapper, FileInfo> implements FileService {

    @Autowired
    FileInfoMapper fileInfoMapper;
    @Override
    public FileInfo getFileJar(Long id) {
        return fileInfoMapper.getfileinfovalue(id);
    }
}
