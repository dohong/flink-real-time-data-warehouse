package com.jolohong.TaskTag.TaskServer.Server.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TaskTag.Mapper.TaskProcessLogMapper;
import com.jolohong.TaskTag.TaskServer.Server.TaskProcessLogService;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcess;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcessLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
@DS("mysql")
public class TaskProcessLogServiceimpl extends ServiceImpl<TaskProcessLogMapper, TaskProcessLog> implements TaskProcessLogService {

    @Override
    public void addTaskProcessLog(TaskProcessLog taskProcessLog) {
        saveOrUpdate(taskProcessLog);
    }
    public void getTaskProcessbyTaskid(Long taskid){

    }
}
