package com.jolohong.TaskTag.TaskServer.Server.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jolohong.TaskTag.Mapper.TaskProcessMapper;
import com.jolohong.TaskTag.TaskServer.Server.TaskProcessService;
import com.jolohong.TaskTag.TaskServer.dao.TaskProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DS("mysql")
public class TaskProcessServiceimpl extends ServiceImpl<TaskProcessMapper, TaskProcess> implements TaskProcessService {

    @Autowired(required = false)
    TaskProcessMapper taskProcessMapper;

    @Override
    public TaskProcess getTaskProcessbyId(Long id) {
        return taskProcessMapper.getTaskProcessById(id);
    }
}
