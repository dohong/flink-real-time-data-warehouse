package com.jolohong.TaskTag.TaskTagMerge;

import com.jolohong.TaskTag.Common.Bean.TagInfo;
import com.jolohong.TaskTag.Common.Dao.TagInfoDao;
import com.jolohong.TaskTag.Common.Utils.PropertiesUtil;
import com.jolohong.TaskTag.Utils.PIVOTUDFT;
import com.jolohong.TaskTag.Utils.collect_set;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class TagMergeProcess {
    private String taskId;
    private String taskDate;
    public TagMergeProcess(String taskId, String taskDate){
        this.taskId=taskId;
        this.taskDate=taskDate;
    }
    public void start() throws SQLException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        System.setProperty("HADOOP_USER_NAME", "jolohong");
        Properties properties = PropertiesUtil.load("conf.properties");
        String hdfs_url = properties.getProperty("hdfs-store.path");
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().useBlinkPlanner().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);
//        tableEnvironment.getConfig().setSqlDialect(SqlDialect.DEFAULT);
        String hiveConfDir="C:\\Users\\jolohong\\IdeaProjects\\houseWare\\UserTagTask\\src\\main\\resources";
        String defaultDataBase="gmalldb";
        String cataLog="catalog";
        HiveCatalog hiveCataLog = new HiveCatalog(cataLog, defaultDataBase, hiveConfDir);
        tableEnvironment.registerCatalog(cataLog, hiveCataLog);
        tableEnvironment.useCatalog(cataLog);

        List<TagInfo> tagInfoList = TagInfoDao.getTagInfoListOnTask();
        String tableName="user_tag_merge_"+taskDate.replace("-","");
        StringBuffer columns = new StringBuffer();
        StringBuffer tablecolumus = new StringBuffer();
        for (int i = 0; i < tagInfoList.size(); i++) {
            columns.append(tagInfoList.get(i).getTag_code().toLowerCase()+" string ");
            tablecolumus.append("tt['"+tagInfoList.get(i).getTag_code().toLowerCase()+"']");
            if(i!=tagInfoList.size()-1){
                columns.append(",");
                tablecolumus.append(",");
            }
        }
        String columnsNames = columns.toString();

        String dropTableSQL=" drop table if exists "+tableName;

        String createTableSQL="create table if not exists \n" +
                "`"+tableName+"` (uid STRING, \n" +
                "" + columnsNames + ")\n" +
                " comment '标签表' \n" +
                "ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' \n" +
                "LOCATION '" +hdfs_url+"tagware/"+tableName+"'";


        StringBuffer unionSQL = new StringBuffer();
        for (int i = 0; i < tagInfoList.size(); i++) {
            unionSQL.append("select uid, '"+tagInfoList.get(i).getTag_code().toLowerCase()+"' as tag_code, tag_value from "+tagInfoList.get(i).getTag_code().toLowerCase());
            if(i!=tagInfoList.size()-1){
                unionSQL.append(" union all ");
            }
        }
        StringBuffer inSQL= new StringBuffer();
        for (int i = 0; i < tagInfoList.size(); i++) {
            inSQL.append("'"+tagInfoList.get(i).getTag_code().toLowerCase()+"'");
            if(i!=tagInfoList.size()-1){
                inSQL.append(",");
            }
        }
        tableEnvironment.createTemporarySystemFunction("collect_list", collect_set.class);
        tableEnvironment.createTemporaryFunction("PIVOTUDFT", PIVOTUDFT.class);
        String selectSQL="select uid,collect_list(MAP[tag_code,tag_value]) as tt \n" +
                " from ("+ unionSQL +") \n" +
                "group by uid  \n";

        String insertSQL="insert into "+tableName+" select * from (select uid,"+tablecolumus.toString()+" from ("+selectSQL+")"+")";

        System.out.println(insertSQL);
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.HIVE);
        tableEnvironment.executeSql(dropTableSQL);
        tableEnvironment.executeSql(createTableSQL);
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.DEFAULT);
        tableEnvironment.executeSql(insertSQL);


    }

}
