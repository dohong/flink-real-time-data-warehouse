package com.jolohong.TaskTag.TaskTreansFrom;

import com.alibaba.fastjson.JSONObject;
import com.jolohong.TaskTag.Common.Bean.TagInfo;
import com.jolohong.TaskTag.Common.Dao.TagInfoDao;
import com.jolohong.TaskTag.Common.Utils.ClickHouseUtils;
import com.jolohong.TaskTag.Common.Utils.SinkClickHouseUtils;
import org.apache.flink.api.common.functions.IterationRuntimeContext;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;
import org.apache.flink.types.Row;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TaskExportCKApp {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME", "jolohong");
        String taskId="1";
        String taskDate="2022-05-11";

        String tableName = "user_tag_merge_" + taskDate.replace("-", "");
        List<TagInfo> tagInfoList = TagInfoDao.getTagInfoListOnTask();
        StringBuffer columns = new StringBuffer();
        for (int i = 0; i < tagInfoList.size(); i++) {
            columns.append(tagInfoList.get(i).getTag_code().toLowerCase()+" String ");
            if(i!=tagInfoList.size()-1){
                columns.append(",");
            }
        }
        String columnsNames = columns.toString();
        System.out.println(columnsNames);
        String dropTableSQL=" drop table if exists "+tableName;
        String createTableSQL = "create table " + tableName + "(uid UInt64, " + columnsNames + ") \n" +
                "engine=MergeTree \n" +
                "primary key uid \n" +
                "order by uid";
        System.out.println(createTableSQL);

        ClickHouseUtils.executeSQL(dropTableSQL);
        ClickHouseUtils.executeSQL(createTableSQL);

        //TODO将数据湖中的数据写入CLickhouse
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(environment,settings);
        String hiveConfDir="C:\\Users\\jolohong\\IdeaProjects\\houseWare\\UserTagTask\\src\\main\\resources";
        String defaultDataBase="gmalldb";
        String cataLog="catalog";
        HiveCatalog hiveCataLog = new HiveCatalog(cataLog, defaultDataBase, hiveConfDir);
        tableEnvironment.registerCatalog(cataLog, hiveCataLog);
        tableEnvironment.useCatalog(cataLog);
        System.out.println(tableName);
        Table table = tableEnvironment.sqlQuery("select * from " + tableName);

       tableEnvironment.toAppendStream(table, Row.class).addSink(new SinkClickHouseUtils("insert into user_profile.user_tag_merge_20220511" +
               "(uid,user_gender,user_ages,todayorder)  values(?,?,?,?)"));

        environment.execute("TaskExportCKApp");
    }
}

