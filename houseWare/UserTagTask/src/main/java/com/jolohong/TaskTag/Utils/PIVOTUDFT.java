package com.jolohong.TaskTag.Utils;

import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

import java.util.Map;

@FunctionHint(
        input = @DataTypeHint("MAP<STRING, STRING>"),
        output = @DataTypeHint("ROW<tag_code STRING, tag_value STRING>"))
public class PIVOTUDFT extends TableFunction<Row> {
    public void eval(Map<String, String> hashMap) {
        // 原来的一行，对应输出一列
        try {
            for (String key : hashMap.keySet()) {
                collect(Row.of(key, hashMap.get(key)));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
