package com.jolohong.TaskTag.Utils;
import org.apache.flink.table.functions.AggregateFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class collect_set extends AggregateFunction<Map<String, String>, collect_set.SumAccumulator> {
    @Override
    public Map<String, String> getValue(SumAccumulator accumulator) {
        Map<String, String> hashMap = new HashMap<>();
        accumulator.list.forEach(hashMap::putAll);
        return hashMap;
    }

    public static class SumAccumulator{
        public List<Map<String, String>> list;
    }

    public void accumulate(SumAccumulator accumulator,Map<String, String> productPrice){
        accumulator.list.add(productPrice);
    }

    @Override
    public collect_set.SumAccumulator createAccumulator() {
        SumAccumulator sumAccumulator = new SumAccumulator();
        sumAccumulator.list=new ArrayList<>();
        return sumAccumulator;
    }
}
