import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

public class flinkhive {
    public static void main(String[] args) {
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().useBlinkPlanner().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);

        String name            = "myhive";
        String defaultDatabase = "gmalldb";
        String hiveConfDir = "C:\\Users\\jolohong\\IdeaProjects\\houseWare\\UserTagTask\\src\\main\\resources";
        HiveCatalog hiveCatalog = new HiveCatalog(name, defaultDatabase, hiveConfDir);
        tableEnvironment.registerCatalog("myhive", hiveCatalog);
        tableEnvironment.useCatalog("myhive");
        tableEnvironment.sqlQuery("select id,user_id,spu_id,is_cancel,partition_days from hudi_users_2_mor1").execute().print();

        tableEnvironment.getConfig().setSqlDialect(SqlDialect.HIVE);
//        tableEnvironment.useDatabase("tagdb");
        tableEnvironment.executeSql("create external table usertag(\n" +
                "id string,\n" +
                "user_id string,\n" +
                "sku_id string,\n" +
                "is_cancenl string)\n" +
                "ROW FORMAT DELIMITED \n" +
                "FIELDS TERMINATED BY '\t' \n" +
                "LINES TERMINATED BY '\n' \n" +
                "location 'hdfs://master:8020/hive/examples'\n");
        String tagSQL="insert into usertag " +
                "select id, user_id, sku_id,is_cancel from hudi_users_2_mor1";
        tableEnvironment.executeSql(tagSQL);
    }
}
