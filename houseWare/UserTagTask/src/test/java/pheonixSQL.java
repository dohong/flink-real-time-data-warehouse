import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.CloseableIterator;

public class pheonixSQL {
    public static void main(String[] args) {
        System.setProperty("HADOOP_USER_NAME","jolohong");
        String userInfoSQL="CREATE TABLE hbase_user_behavior(\n" +
                "rowkey STRING,\n" +
                "info ROW<user_id STRING,mt_wm_poi_id STRING,shop_name STRING,source STRING,platform STRING,create_time STRING,dt STRING,hr STRING,mm STRING>,\n" +
                "PRIMARY KEY (rowkey) NOT ENFORCED\n" +
                ") WITH (\n" +
                "'connector' = 'hbase-2.2',\n" +
                "'table-name' = 'hbase_user_behavior',\n" +
                "'zookeeper.quorum' = 'master:2181,slave001:2181',\n" +
                "'zookeeper.znode.parent' = '/hbase'\n" +
                ")";
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        EnvironmentSettings build = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
        StreamTableEnvironment tableEnvironment = StreamTableEnvironment.create(environment);
        tableEnvironment.getConfig().setSqlDialect(SqlDialect.DEFAULT);
        tableEnvironment.executeSql(userInfoSQL);
        tableEnvironment.executeSql("INSERT INTO hbase_user_behavior\n" +
                "SELECT 'rowkey_test', ROW('test001', 'test', 'test','test','test','test','test','test','test') as info ");
        tableEnvironment.sqlQuery("SELECT * FROM hbase_user_behavior").execute().print();

    }
}
