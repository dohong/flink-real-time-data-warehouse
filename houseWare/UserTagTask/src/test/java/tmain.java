import com.jolohong.TaskTag.TaskServer.Server.impl.TaskProcessServiceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class tmain {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date()).toString();
        System.out.println(format);
        Date parse = simpleDateFormat.parse(format);
        System.out.println(parse);
    }
}
