package com.jolohong.gmall;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GmallLoggerMain {
    public static void main(String[] args) {
        SpringApplication.run(GmallLoggerMain.class, args);
    }
}
