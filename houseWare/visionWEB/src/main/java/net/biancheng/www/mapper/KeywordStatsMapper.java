package net.biancheng.www.mapper;

import net.biancheng.www.Entity.been.KeywordStats;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
public interface KeywordStatsMapper {

//    @Select("select \n" +
//            "       keyword,\n" +
//            "       sum(keyword_stats_2022.ct * multiIf(\n" +
//            "           source ='SEARCH',10,\n" +
//            "           source ='ORDER',5,\n" +
//            "           source ='CART',2,\n" +
//            "           source ='CLICK',1,0\n" +
//            "           ))  ct\n" +
//            "       from keyword_stats_2022\n" +
//            "where toYYYYMMDD(stt) = #{date}\n" +
//            "group by keyword\n" +
//            "order by sum(keyword_stats_2022.ct) \n" +
//            "limit #{limit};")
    @Select("SELECT ks.keyword , SUM(ks.ct) ct\n" +
            "from gmalldb.keyword_stats_2022 ks \n" +
            "WHERE toYYYYMMDD(ks.stt)=#{date}\n" +
            "GROUP BY \n" +
            "ks.keyword \n" +
            "order by sum(ks.ct) desc\n" +
            "LIMIT #{limit};")
    List<KeywordStats> selectKeywordsStats(@Param("date") int date, @Param("limit") int limit);

}
