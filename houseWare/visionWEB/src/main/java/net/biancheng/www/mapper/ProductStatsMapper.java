package net.biancheng.www.mapper;

import net.biancheng.www.Entity.been.ProductStats;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

/*
商品统计Mapper
 */
public interface ProductStatsMapper {

    //获取商品交易额
    @Select("select sum(order_amount) order_amount from product_stats_2022 where toYYYYMMDD(stt)=#{date}")
    BigDecimal getGMV(int date);


    //统计某天不同SPU商品交易排名
    @Select("select spu_id,spu_name,sum(order_amount) order_amount,sum(order_ct) order_ct from product_stats_2022 " +
            "where toYYYYMMDD(stt)=#{date} group by spu_id,spu_name having order_amount>0 order by order_amount desc limit #{limit}")
    List<ProductStats> getProductStatsGroupBySpu(@Param("date") int date, @Param("limit") int limit);


    //统计某天不同类别商品交易额排名
    @Select("select category3_id,category3_name,sum(order_amount) order_amount  from product_stats_2022 where toYYYYMMDD(stt) =#{date} " +
            "group by category3_id,category3_name having  order_amount >0 order by  order_amount desc limit #{limit}")
    List<ProductStats> getProductStatsGroupByCategory3(@Param("date") int date, @Param("limit") int limit);

    //统计某天不同品牌商品交易额排名
    @Select("select tm_id,tm_name,sum(order_amount) order_amount from product_stats_2022 where toYYYYMMDD(stt) =#{date} group by tm_id,tm_name having order_amount >0 order by order_amount desc  limit #{limit} \n")
    List<ProductStats> getProductStatsByTrademark(@Param("date") int date, @Param("limit") int limit);


    @Select("SELECT  ps.spu_name , SUM(ps.favor_ct) as favor_ct ,\n" +
            "SUM(ps.cart_ct) as cart_ct ,SUM(ps.refund_order_ct) as refund_order_ct ,\n" +
            "SUM(ps.order_ct) as order_ct \n" +
            "FROM gmalldb.product_stats_2022 ps \n" +
            "WHERE toYYYYMMDD(ps.stt)=#{date}\n" +
            "GROUP BY \n" +
            "ps.spu_name \n" +
            "ORDER by SUM(ps.favor_ct) DESC ,SUM(ps.cart_ct) DESC, \n" +
            "SUM(ps.refund_order_ct) DESC , SUM(ps.refund_order_ct) DESC  \n" +
            "LIMIT #{limit};")
    List<ProductStats> getProductStatsByCount(int date, int limit);
}
