package net.biancheng.www.mapper;

import net.biancheng.www.Entity.been.VisitorStats;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
访客流量统计mapper
 */
public interface VisitorStatsMapper {

    // 新老访客流量统计
    @Select("select is_new,sum(uv_ct) uv_ct,sum(pv_ct) pv_ct," +
            "sum(sv_ct) sv_ct, sum(uj_ct) uj_ct,sum(dur_sum) dur_sum " +
            "from visitor_stats_2022 where toYYYYMMDD(stt)=#{date} group by is_new")
    List<VisitorStats> selectVisitorStatsByNewFlag(int date);


    //分时流量统计
    @Select("select sum(if(is_new='1', visitor_stats_2022.uv_ct,0)) new_uv,toHour(stt) hr," +
            "sum(visitor_stats_2022.uv_ct) uv_ct, sum(pv_ct) pv_ct, sum(uj_ct) uj_ct " +
            "from visitor_stats_2022 where toYYYYMMDD(stt)=#{date} group by toHour(stt)")
    List<VisitorStats> selectVisitorStatsByHour(int date);


    @Select("select count(pv_ct) pv_ct from visitor_stats_2022 " +
            "where toYYYYMMDD(stt)=#{date} ")
    Long selectPv(int date);

    @Select("select count(uv_ct) uv_ct from visitor_stats_2022 " +
            "where toYYYYMMDD(stt)=#{date} ")
    Long seelctUv(int date);

    @Select("select  toDate(vs.stt) as stt,SUM(if(vs.is_new='1', vs.uv_ct,0)) as new_uv\n" +
            ",SUM(vs.uv_ct) as uv_ct ,SUM(vs.pv_ct) as pv_ct , SUM(vs.uj_ct) as uj_ct  \n" +
            "FROM gmalldb.visitor_stats_2022 vs \n" +
            "WHERE toYYYYMMDD(vs.stt)>=#{startDate} and toYYYYMMDD(vs.edt)<=#{endDate} \n" +
            "GROUP by toDate(vs.stt)")
    List<VisitorStats> selectVistorStatsByDate(int startDate, int endDate);
}
