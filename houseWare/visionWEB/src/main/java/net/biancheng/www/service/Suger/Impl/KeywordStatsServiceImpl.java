package net.biancheng.www.service.Suger.Impl;

import net.biancheng.www.Entity.been.KeywordStats;
import net.biancheng.www.mapper.KeywordStatsMapper;
import net.biancheng.www.service.Suger.KeywordStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeywordStatsServiceImpl implements KeywordStatsService {

    @Autowired
    KeywordStatsMapper keywordStatsMapper;


    @Override
    public List<KeywordStats> getKeywordStats(int date, int limit) {
        return keywordStatsMapper.selectKeywordsStats(date,limit);
    }
}
