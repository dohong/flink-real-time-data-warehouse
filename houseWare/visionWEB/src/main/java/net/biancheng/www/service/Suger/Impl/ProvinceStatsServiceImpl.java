package net.biancheng.www.service.Suger.Impl;

import net.biancheng.www.Entity.been.ProvinceStats;
import net.biancheng.www.mapper.ProvinceStatsMapper;
import net.biancheng.www.service.Suger.ProvinceStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinceStatsServiceImpl implements ProvinceStatsService {
    @Autowired
    ProvinceStatsMapper provinceStatsMapper;


    @Override
    public List<ProvinceStats> getProvinceStats(int date) {
        return provinceStatsMapper.selectProvinceStats(date);
    }
}
