package net.biancheng.www.service.Suger.Impl;

import net.biancheng.www.Entity.been.VisitorStats;
import net.biancheng.www.mapper.VisitorStatsMapper;
import net.biancheng.www.service.Suger.VisitorStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitorStatsServiceImpl implements VisitorStatsService {

    @Autowired
    VisitorStatsMapper visitorStatsMapper;


    @Override
    public List<VisitorStats> getVisitorStatsByNewFlag(int date) {
        return  visitorStatsMapper.selectVisitorStatsByNewFlag(date);
    }

    @Override
    public List<VisitorStats> getVisitorStatsByHour(int date) {
        return visitorStatsMapper.selectVisitorStatsByHour(date);
    }

    @Override
    public Long getPv(int date) {
        return visitorStatsMapper.selectPv(date);
    }

    @Override
    public Long getUv(int date) {
        return visitorStatsMapper.seelctUv(date);
    }

    @Override
    public List<VisitorStats> selectVistorStatsByDate(int startDate, int endDate) {
        return visitorStatsMapper.selectVistorStatsByDate(startDate, endDate);
    }

}
