package net.biancheng.www.service.Suger;

import net.biancheng.www.Entity.been.KeywordStats;

import java.util.List;

public interface KeywordStatsService {
    List<KeywordStats> getKeywordStats(int date, int limit);
}
