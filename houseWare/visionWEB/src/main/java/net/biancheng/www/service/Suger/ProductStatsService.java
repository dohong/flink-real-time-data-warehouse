package net.biancheng.www.service.Suger;
import net.biancheng.www.Entity.been.ProductStats;
import org.apache.ibatis.annotations.Param;


import java.math.BigDecimal;
import java.util.List;

public interface ProductStatsService {
    //获取某一天的总交易额
    BigDecimal getGMV(int date);

    List<ProductStats> getProductStatsGroupBySpu(int date, int limit);

    List<ProductStats> getProductStatsGroupByCategory3(int date, int limit);

    List<ProductStats> getProductStatsByTrademark(int date, int limit);

    List<ProductStats> getProductStatsByCount(int date, int limit);
}
