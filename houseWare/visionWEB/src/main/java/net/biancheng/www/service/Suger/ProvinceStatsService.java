package net.biancheng.www.service.Suger;

import net.biancheng.www.Entity.been.ProvinceStats;

import java.util.List;

public interface ProvinceStatsService {

    List<ProvinceStats> getProvinceStats(int date);
}
