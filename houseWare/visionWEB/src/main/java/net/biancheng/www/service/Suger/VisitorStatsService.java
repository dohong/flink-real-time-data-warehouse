package net.biancheng.www.service.Suger;

import net.biancheng.www.Entity.been.VisitorStats;

import java.util.List;

public interface VisitorStatsService {
    List<VisitorStats> getVisitorStatsByNewFlag(int date);

    List<VisitorStats> getVisitorStatsByHour(int date);

    Long getPv(int date);
    Long getUv(int date);

    List<VisitorStats> selectVistorStatsByDate(int startDate, int endDate);
}
