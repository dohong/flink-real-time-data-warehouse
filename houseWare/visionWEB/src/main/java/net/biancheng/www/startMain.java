package net.biancheng.www;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages = "net.biancheng.www.mapper")
public class startMain {
    public static void main(String[] args) {
        SpringApplication.run(startMain.class, args);
    }
}
